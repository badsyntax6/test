
var leftoverJson = null;

(function($) {
    $.fn.deserialize = function(options) {

        var settings = $.extend({
            form : '#' + $(this).attr('id'),
            json : null,
            getJson : null,
            vars : null
        }, options);

        var action = {
            
            getJson:function() {
                if (settings.json == null && settings.getJson != null) {
                    $.ajax({
                        url: settings.getJson,
                        type: 'GET',
                        async: false,
                        success: function(response) {
                            // $('body').prepend(response);
                            if ($.trim(response)) {
                                settings.json = JSON.parse(response);
                            }
                        }
                    });
                }
            },

            populateForm:function() {

                var type = null;

                $(settings.form + ' input, ' + settings.form + ' select, ' + settings.form + ' textarea').each(function() {

                    var self = $(this);
                    var name = $(this).prop('name');
                    type = $(this).prop('type');
                    
                    $.each(settings.json, function(k, v) {

                        if (k == name) {
                            switch (type) {
                                case 'checkbox':
                                    $('input[name="' + k + '"]').prop('checked', true);
                                break;
                                case 'radio':
                                    if (self.val() && self.val() == v) {
                                        self.prop('checked', true);
                                    }
                                break;  
                                case 'select-one':
                                    $('select[name="' + k + '"]').val(v);
                                break;
                                case 'textarea':
                                    $('textarea[name="' + k + '"]').val(v);
                                break;
                                case 'text':
                                case 'tel':
                                case 'date':
                                case 'time':
                                case 'email':
                                case 'hidden':
                                case 'password':
                                case 'color':
                                    $('input[name="' + k + '"]').attr('value', v);
                                break;
                            }
                        }
                    });

                });

            },

            createLeftoverJson:function() {
                $(settings.form + ' input, ' + settings.form + ' select, ' + settings.form + ' textarea').each(function() {

                    var self = $(this);
                    var name = $(this).prop('name');
                    type = $(this).prop('type');
                    
                    $.each(settings.json, function(i, v) {

                        if (i == name) {
                            if (self.val() && self.val().length > 0) {
                                delete settings.json[i];
                            }
                        }
                    });

                });

                leftoverJson = settings.json;

                return leftoverJson;
            }
        }

        return this.each(function() {    

            action.getJson();
            action.populateForm();
            action.createLeftoverJson();
            
        });
    }
}(jQuery));