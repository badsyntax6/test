/**
 * Pagination Controls 
 *
 * 
 */
var MainControls = new Object();

/**
 * Pagination Controls Initializer
 *
 * This is the primary funciton for this object. It will set 
 * the properties and call all other functions.
 */
MainControls.init = function() {
    MainControls.url = window.location.pathname.split('/');
    MainControls.view = MainControls.url[1];
    MainControls.ids = [];

    MainControls.deleteRecords();
    MainControls.userControls();
}

/**
 * Delete Records
 * 
 * This function will delete all the checked off items in the list.
 * Records are deleted by (id) so this function will work across 
 * the board for any list.
 */
MainControls.deleteRecords = function() {
    $('body').on('click', '.btn-mass-delete', function() {
        var ids = $('.checkbox').serializeArray();
        $.post('/' + MainControls.view + '/delete', ids, function(response) {
            console.log(response);
            if ($.trim(response)) {
                var json = JSON.parse(response);
                $('.alert-area').append('<div class="alert ' + json.alert + '"><strong>' + json.alert + '!</strong> ' + json.message + ' <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button>');
                Pagination.refreshTable();
            }
        });
    });
}

MainControls.userControls = function() {
    $('#user-group').on('change', function() {
        var ids = $('.checkbox').serializeArray();
        MainControls.editUser(ids, $(this).val());
    });
}

MainControls.editUser = function(ids, group) {
    var data = {ids: ids, group: group};
    $.post('/users/edit', data, function(response) {
        $('.alert').remove();
        $('#group-none').prop('selected', true);
        if ($.trim(response)) {
            var json = JSON.parse(response);
            $('.alert-area').append('<div class="alert ' + json.alert + '"><strong>' + json.alert + '!</strong> ' + json.message + ' <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button>');
            Pagination.refreshTable();
        }
    }).always(function() {
        ids = [];
        $('.btn-list-control').prop('disabled', true);
    });
}