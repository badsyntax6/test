

class Deserialize 
{
    constructor(o)
    {
        this.o = o;
        this.leftovers = null;
        this.get();
        return new Promise((resolve,reject) => {
            // do some async task
            resolve(this.leftovers);
        });
    }

    get()
    {
        console.log(1);
        let parent = this;
        return axios.get(this.o.url).then(function(response) {
            parent.o.data = response.data;
            console.log(2);
            parent.populate();
        });
    }

    populate()
    {
        console.log(3);
        let parent = this;
        let inputs = document.querySelectorAll(this.o.form + ' input, ' + this.o.form + ' textarea, ' + this.o.form + ' select');

        inputs.forEach(function(v, i) {
            let name = v.name;
            let type = v.type;
            let data = parent.o.data;
        
            Object.keys(data).forEach(function(i) {
                if (i == name) {
                    switch (type) {
                        case 'checkbox':
                            v.checked = true;
                        break;
                        case 'radio':
                            if (v.value == data[i]) v.checked = true;
                        break;  
                        case 'select-one':
                        case 'textarea':
                        case 'text':
                        case 'tel':
                        case 'date':
                        case 'time':
                        case 'email':
                        case 'hidden':
                        case 'password': 
                        case 'color':
                            v.value = data[i];
                        break;
                    }

                    delete parent.o.data[i];
                }
            }); 
        });

        this.leftovers = parent.o.data;
    }
}