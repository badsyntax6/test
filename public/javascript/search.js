
$(document).on('keyup', '#search-string', function() {
    if ($('#search-string').val().length > 1) {
        $('.search-results').css({'display':'block'});
        Common.showLoader('.search-results');
        $.post('/search', $('#search-string').serialize(), function(response) {
            if ($.trim(response)) {
                Common.removeLoader();
                $('.search-results li').remove('');
                var json = JSON.parse(response);
                var users = json.users;
                if (users) {
                    $('.search-results-users em').text('Users:');
                    $.each(users, function(k, v) {
                        $('.search-results-users').append('<li><a href="/users/user/' + v.firstname.toLowerCase() + '-' + v.lastname.toLowerCase() + '">' + v.firstname + ' ' + v.lastname + ' (' + v.email + ')</a></li>');
                    });
                } else {
                    $('.search-results-users li').remove('');
                    $('.search-results-users em').text('');
                }
            }
        });
    } else {
        $('.search-results').css({'display':'none'});
    }
});
