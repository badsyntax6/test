/**
 * Notfier
 */
var Notifier = new Object();

$(window).on('load', function() {
    Object.keys(Notifier).forEach(function(key) {
        var func = Notifier[key];
        if (typeof func === 'function' && key != 'colorStatusText') {
            try {
                func();
            } catch (error) {
                $('body').prepend(error.stack);
                window.onerror = function(error, file, line, col) {
                    $('body').prepend(error + ' File: ' + file + ' Line: ' + line + ' Col: ' + col);
                    return false;
                };
            }
        }
    });  
});

Notifier.init = function() {
    //Increment the idle time counter every minute.
    setInterval(Notifier.checkTime, 60000); // 1 Min

    $('body').on('click', '.btn-close-notification', function() {
        $(this).parent().remove();
    });

    $('body').on('click', '.btn-acknowledge', function() {
        var request_id = $(this).parent().parent().find('.request-id').text();
        var parent = $(this).parent().parent();
        var data = {request_id : request_id};

        $.post('/requests/acknowledgeRequest', data, function(response) {
            if ($.trim(response)) {
                var json = JSON.parse(response);
                if (json.alert == 'success') {
                    parent.remove();
                } else {
                    alert('fail');
                }
            }
        });
    });
}

Notifier.checkTime = function() {
    $.get('/requests/checkRequests', function(response) {
        $('.notification-wrapper').remove();
        $('.notification').remove();
        $('body').prepend('<div class="notification-wrapper"></div>');
        if ($.trim(response)) {
            var json = JSON.parse(response);
            $(json).each(function() {
                $('.notification-wrapper').append('<div class="notification"><button type="button" class="btn-close-notification"><i class="fas fa-times fa-fw"></i></button><div class="row request-id">Request: #' + this.requests_id + '</div><div class="row">Status: <span class="req-status">' + this.status + '</span></div><div class="row"><a href="/requests/request/' + this.requests_id + '" class="btn btn-small">View</a> <button type="button" class="btn btn-small btn-acknowledge no-load">Acknowledge</button></div></div>');
                });
            $('.req-status').each(function() {
                Notifier.colorStatusText($(this));
            });                
        }
    })
}

Notifier.colorStatusText = function(status) {
    switch (status.text()) {
        case 'APPROVED':
            status.css({'color' : '#76d769'});
            break;
        case 'DENIED':
            status.css({'color' : '#ff6868'}); 
            break;
        case 'PENDING':
            status.css({'color' : '#ffee00'});
            break;
        default:
            
            break;
    }
}