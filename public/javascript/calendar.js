
/**
 * Calendar
 *
 * The Calendar takes json data from the calendar controller and 
 * displays it on the page
 *
 * Example
 *      $('#calendar-table').calendar({
 *          view : 'month',
 *          post : '/calendar/get-events'
 *      });
 * 
 * SEE: Calendar Controller Class - /root/private/controllers/CalendarController.php
 */
(function($) {
    $.fn.calendar = function(options) {

        var settings = $.extend({
            table : $(this),    // table element to fill with calendar. DEFAULT: $(this)
            date : null,        // display a specific calendar view based on this date. DEFAULT: null
            view : 'month',     // day | week | month. DEFAULT: month
            post : null,        // url to get calendar json data from. DEFAULT: null
            day_click : true    // when in month view make the day button clickable to jump to that day. DEFAULT: true
        }, options);

        var next;
        var prev;
        var calendar;

        var action = {
            drawMonthView:function(date = settings.date) {
                $.post('/calendar/getCalendar', {date : date}, function(response) {
                    if ($.trim(response)) {
                        calendar = JSON.parse(response);
                        prev = calendar.last_month;
                        next = calendar.next_month;

                        action.reset(calendar.month + ' ' + calendar.year);
                        settings.table.prepend('<tr><th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th></tr>');
                        settings.table.attr('data-view', 'month');
                        settings.date = calendar.month + ' ' + calendar.year;

                        var week = '';
                        var lm_end = parseInt(calendar.lm_day_count);
                        var lm_start = lm_end - calendar.dow + 1;

                        for (var day = lm_start; day <= lm_end; day++) {
                            week = week + '<td class="om">' + day + '</td>';
                        }

                        for (var day = 1; day <= calendar.day_count; day++, calendar.dow++) {
                            var date = Date.parse(calendar.year + '-' + calendar.month_num + '-' + day + ' ' + '00:00:00');
                            var todays_date = Date.parse(calendar.todays_date_iso + ' ' + '00:00:00');
                            if (todays_date == date) {
                                week = week + '<td data-date="' + date + '" class="today cm"><button type="button" class="btn btn-cal-day no-load">' + day + '</button><div class="events">';
                            } else {
                                week = week + '<td data-date="' + date + '" class="cm"><button type="button" class="btn btn-cal-day no-load">' + day + '</button><div class="events">';
                            }
                            week = week + '</div></td>';
                            
                            if (calendar.dow % 7 == 6 || day == calendar.day_count) {
                                if (day == calendar.day_count) {
                                    var cells = 6 - (calendar.dow % 7);
                                    for (var num = 1; num <= cells; num++) {
                                        week = week + '<td class="nm">' + num + '</td>';
                                    }
                                }
                                settings.table.append('<tr class="calendar-week">' + week + '</tr>');
                                week = '';
                            }
                        }
                    }
                }).always(function() {
                    if (!date) {
                        date = calendar.todays_date;
                    }
                    action.getEvents(date);
                });
            },
            drawWeekView:function(date = null) {
                $.post('/calendar/getCalendar', {date : date}, function(response) {
                    if ($.trim(response)) {
                        calendar = JSON.parse(response);
                        var week = [];
                        var lm_end = parseInt(calendar.lm_day_count);
                        var lm_start = lm_end - calendar.dow + 1;
                        var tm = calendar.month_year;
                        var lm = calendar.last_month;
                        var nm = calendar.next_month;

                        settings.table.attr('data-view', 'week');

                        for (var day = lm_start; day <= lm_end; day++) {
                            week.push(day + ' ' + lm);
                        }

                        for (var day = 1; day <= calendar.day_count; day++, calendar.dow++) {
                            week.push(day + ' ' + tm);
                            if (calendar.dow % 7 == 6 || day == calendar.day_count) {
                                if (day == calendar.day_count) {
                                    var cells = 6 - (calendar.dow % 7);
                                    for (var num = 1; num <= cells; num++) {
                                        week.push(num + ' ' + nm);
                                    }
                                }
                            }
                        }

                        var weeks = [];
                        var w1 = week.slice(0, 7), w2 = week.slice(7, 14), w3 = week.slice(14, 21), w4 = week.slice(21, 28), w5 = week.slice(28, 35), w6 = week.slice(35, 42);
                        var week_start;
                        var week_end;
                        var w;

                        weeks.push(w1, w2, w3, w4, w5, w6);

                        $.each(weeks, function(k, v) {
                            if (!w) {
                                if ($.inArray(calendar.day + ' ' + tm, v) > -1) {
                                    w = v;
                                }
                            }
                        });

                        var lw = parseInt(w[0].split(' ')[0]);
                        var lwm = w[0].split(' ')[1] + ' ' + w[0].split(' ')[2];
                        var nw = parseInt(w[6].split(' ')[0]);
                        var nwm = w[6].split(' ')[1] + ' ' + w[6].split(' ')[2];

                        prev = lw - 1 + ' ' + tm;

                        if (lw == 1) {
                            prev = calendar.lm_day_count + ' ' + calendar.last_month;
                        }
                        if (lwm == lm) {
                            prev = lw - 1 + ' ' + calendar.last_month;
                        }

                        next = nw + 1 + ' ' + tm;

                        if (nw == 31) {
                            next = 1 + ' ' + calendar.next_month;
                        }
                        if (nwm == nm) {
                            next = parseInt(w[6].split(' ')[0]) + 1 + ' ' + calendar.next_month;
                        }

                        week_start = w[0];
                        week_end = w[6];

                        if (week_start.split(' ')[1] == week_end.split(' ')[1]) {
                            week_start = week_start.split(' ')[0];
                        } else {
                            week_start = week_start.split(' ')[0] + ' ' + week_start.split(' ')[1];
                        }

                        action.reset(week_start + ' - ' + week_end);
                        settings.table.append('<tr><th></th><th>Sun ' + w[0].split(' ')[0] + '</th><th>Mon ' + w[1].split(' ')[0] + '</th><th>Tue ' + w[2].split(' ')[0] + '</th><th>Wed ' + w[3].split(' ')[0] + '</th><th>Thu ' + w[4].split(' ')[0] + '</th><th>Fri ' + w[5].split(' ')[0] + '</th><th>Sat ' + w[6].split(' ')[0] + '</th></tr>');
                        settings.date = calendar.month + ' ' + calendar.year;

                        var time = 0;
                        var row = '';
            
                        for (var hour = 0; hour <= 11; hour++, time++) {
                            row = '<tr><td class="time-td">' + hour + 'am</td>';
                            $.each(w, function(k, v) {
                                row = row + '<td data-day="' + Date.parse(new Date(v)) + '" data-time="' + time + '"><div class="events"></div></td>';
                            });
                            row = row + '</tr>';
                            settings.table.append(row);
                            row = '';
                        }

                        time = 12;

                        for (var hour = 0; hour <= 11; hour++, time++) {
                            row = '<tr><td class="time-td">' + hour + 'pm</td>';
                            $.each(w, function(k, v) {
                                row = row + '<td data-day="' + Date.parse(new Date(v)) + '" data-time="' + time + '"><div class="events"></div></td>';
                            });
                            row = row + '</tr>';
                            settings.table.append(row);
                            row = '';
                        }

                        $('td:first-child').each(function() {
                            if ($(this).text() == '0am') {
                                $(this).text('12am')
                            }
                            if ($(this).text() == '0pm') {
                                $(this).text('12pm')
                            }
                        });
                    }
                }).always(function() {
                    if (!date) {
                        date = calendar.todays_date;
                    }
                    action.getEvents(date);
                });
            },
            drawDayView:function(date = null) {
                $.post('/calendar/getCalendar', {date : date}, function(response) {
                    if ($.trim(response)) {
                        calendar = JSON.parse(response);
                        var yesterday = parseInt(calendar.day) - 1;
                        var tomorrow = parseInt(calendar.day) + 1;
                        var my = calendar.month + ' ' + calendar.year;
                        var lm = calendar.last_month;
                        var nm = calendar.next_month;
                        var day_count = parseInt(calendar.day_count);

                        prev = yesterday + ' ' + my;
                        next = tomorrow + ' ' + my;

                        if (yesterday == 0) {
                            yesterday = calendar.lm_day_count;
                            prev = yesterday + ' ' + lm;
                        }

                        if (tomorrow > day_count) {
                            tomorrow = 1;
                            next = tomorrow + ' ' + nm;
                        }
                        
                        action.reset(calendar.day + ' ' + calendar.month + ' ' + calendar.year);

                        settings.table.attr('data-view', 'day');
                        settings.table.prepend('<th></th><th colspan="8">' + calendar.today + '</th>');
                        settings.table.append('<tr><td class="time-td">12am</td><td data-time="0"><div class="events"></div></td></tr>');
            
                        var time = 1;
            
                        for (var hour = 1; hour <= 11; hour++, time++) {
                            col = '<td class="time-td">' + hour + 'am' + '</td><td data-time="' + time + '"><div class="events"></div></td>';
                            settings.table.append('<tr>' + col + '</tr>');
                        }
            
                        settings.table.append('<tr><td class="time-td">12pm</td><td data-time="12"><div class="events"></div></td></tr>');

                        time = 13;
            
                        for (var hour = 1; hour <= 11; hour++, time++) {
                            col = '<td class="time-td">' + hour + 'pm' + '</td><td data-time="' + time + '"><div class="events"></div></td>';
                            settings.table.append('<tr>' + col + '</tr>');
                        }
                    }
                }).always(function() {
                    if (!date) {
                        date = calendar.todays_date;
                    }
                    action.getEvents(date);
                });
            },
            reset:function(title) {
                $('.this-month').text(title);
                $('.calendar').removeAttr('id');
                $('.calendar th').remove();
                $('.calendar tr').remove();
                $('.week').remove();
                $('.calendar-week').remove();
            },
            navigate:function() {
                $('body').on('click', '.btn-prev', function() {
                    if ($('table').attr('data-view') == 'day') {
                        action.drawDayView(prev);   
                    }
                    if ($('table').attr('data-view') == 'week') {
                        action.drawWeekView(prev);
                    }
                    if ($('table').attr('data-view') == 'month') {
                        action.drawMonthView(prev);  
                    } 
                });
            
                $('body').on('click', '.btn-next', function() {
                    if ($('table').attr('data-view') == 'day') {
                        action.drawDayView(next);   
                    }
                    if ($('table').attr('data-view') == 'week') {
                        action.drawWeekView(next);
                    }
                    if ($('table').attr('data-view') == 'month') {
                        action.drawMonthView(next);  
                    } 
                });

                $('body').on('click', '.btn-today', function() {
                    if ($('table').attr('data-view') == 'day') {
                        action.drawDayView(null);   
                    }
                    if ($('table').attr('data-view') == 'week') {
                        action.drawWeekView(null);   
                    }
                    if ($('table').attr('data-view') == 'month') {
                        action.drawMonthView(null);  
                    }  
                });

                $('body').on('click', '.btn-month', function() {
                    if ($('table').attr('data-view') != 'month') {
                        action.drawMonthView(null);  
                    }
                });
            
                $('body').on('click', '.btn-week', function() {
                    if ($('table').attr('data-view') != 'week') {
                        action.drawWeekView();
                    }   
                });
            
                $('body').on('click', '.btn-day', function() {
                    if ($('table').attr('data-view') != 'day') {
                        action.drawDayView();   
                    }
                });

                $('body').on('click', '.btn-cal-day, th', function() {
                    if (settings.day_click) {
                        var day = ($(this).text());
                        action.drawDayView(day + ' ' + settings.date);      
                    }
                });
            },
            delete:function() {
                $(document).on('click', '.btn-del-event', function() {
                    var id = $(this).attr('data-id');
                    if (confirm('Are you sure you want to delete this event?')) {
                        var view = $('#calendar-table').attr('data-view');
                        $.post('/calendar/delete', {id : id}, function(response) {
                            var view = $('#calendar-table').attr('data-view');
                            if (view == 'month') action.drawMonthView();
                            if (view == 'week') action.drawWeekView();
                            if (view == 'day') action.drawDayView();
                        });
                    }
                });
            },
            events:function() {
                $(document).on('click', '.event', function() {
                    $('.event-large').remove();
                    
                    var id = $(this).attr('data-id');
                    var self = $(this);
                    var style = $(this).attr('style');

                    $.post('/calendar/getEvent', {id : id}, function(response) {
                        if ($.trim(response)) {
                            var json = JSON.parse(response);
                            var event = `<div class="event-large">
                                            <div class="event-large-header">
                                            <i class="fas fa-calendar-alt fa-fw"></i> ` + json.title + ` 
                                            <button type="button" class="btn-del-event" data-id="` + id + `"><i class="far fa-trash-alt fa-fw"></i> Delete</button>
                                            <a href="/calendar/edit/` + id + `"><i class="fas fa-pencil-alt fa-fw"></i> Edit</a>
                                            </div>
                                            <p>` + json.description + `<span><i class="far fa-clock fa-fw"></i> ` + json.start_time + ` - ` + json.end_time + `</span></p>
                                        </div>`;
                            
                            $('.event-large', self).text('');
                            self.append(event);     
                            
                            $('.event-large-header').attr('style', style + 'border:none;');
                            $('.event-large-header')
                        }
                    });
                });
            
                $(document).on('click', 'body', function() {
                    if ($('.event-large').length > 0) {
                        $('.event-large').remove();
                    }
                });
            },
            getEvents:function(date) {
                $.post(settings.post, {date : date}, function(response) {
                    if ($.trim(response)) {
                        var json = JSON.parse(response);
                        var data = json.data;
                        var this_cell;
                        var start_day;
                        var end_day;
                        var start_month;
                        var time;
                        var start_time;
                        var end_time;
                        var view = $('table').attr('data-view');
                        var event;
                        var week_day;
                        var today;
                        var event_start;
                        var event_end;
                        var border;
                        
                        if (view == 'month') {
                            $('td.cm').each(function() {
                                this_cell = $(this);
                                this_day = $(this).attr('data-date');
                                
                                $(data).each(function(k, v) {
                                    start_day = parseInt(v.start_day);
                                    end_day = parseInt(v.end_day);
                                    start_month = Date.parse(v.start_month_iso);
                                    last_month = Date.parse(v.last_month_iso);
                                    event_start = Date.parse(v.start_month_iso + '-' + start_day + ' ' + '00:00:00');
                                    event_end = Date.parse(v.end_month_iso + '-' + end_day + ' ' + '00:00:00');
                                    border = adjustCardColors(v.background);
                                    event = `<div class="event" data-id="` + v.id + `" style="color:` + v.color + `;background-color:` + v.background + `; border-left:5px solid ` + border + `;">
                                                <p>` + v.title + ` - ` + v.description + ` ` + v.start_time_short + ` 
                                                    <span>` + v.start_day + `/` + v.start_month.replace(' ', '/') + ` ` + v.start_time + ` - ` + v.end_day + `/` + v.end_month.replace(' ', '/') + ` ` + v.end_time + `</span>
                                                </p>
                                            </div>`;
                                    
                                    if (event_start <= this_day && this_day <= event_end) {
                                        this_cell.find('.events').append(event);
                                    }
                                });
                            });
                        }

                        if (view == 'week') {
                            $('td').each(function() {
                                if (!$(this).hasClass('time-td')) {
                                    this_cell = $(this);
                                    week_day = parseInt(this_cell.attr('data-day'));
                                    time = this_cell.attr('data-time');

                                    $(data).each(function(k, v) {
                                        start_month = Date.parse(v.start_month_iso);
                                        last_month = Date.parse(v.last_month_iso);
                                        start_day = Date.parse(v.start_month_iso + '-' + v.start_day + ' ' + '00:00:00');
                                        end_day = Date.parse(v.end_month_iso + '-' + v.end_day + ' ' + '00:00:00');
                                        time = parseInt(time);
                                        start_time = parseInt(v.start_time_mil);
                                        end_time = parseInt(v.end_time_mil);
                                        border = adjustCardColors(v.background);
                                        event = `<div class="event" data-id="` + v.id + `" style="color:` + v.color + `;background-color:` + v.background + `; border-left:3px solid ` + border + `;">
                                                    <p>` + v.title + ' - ' + v.description + ` ` + v.start_time_short + ` <span>` + v.start_day + `/` + v.start_month.replace('-', '/') + ' ' + v.start_time + ` - ` + v.end_time + `</span></p>
                                                </div>`;
                                        
                                        if (start_month == last_month) {
                                            start_day = Date.parse(v.start_month_iso + '-' + 1 + ' ' + '00:00:00');
                                        } 

                                        if (week_day >= start_day && week_day <= end_day) {
                                            if (time >= start_time && time <= end_time) {
                                                this_cell.find('.events').append(event);
                                            }
                                        }
                                    });
                                }
                            });
                        }

                        if (view == 'day') {
                            $('td').each(function() {
                                if (!$(this).hasClass('time-td')) {
                                    this_cell = $(this);
                                    time = parseInt(this_cell.attr('data-time'));
                                    
                                    $(data).each(function(k, v) {
                                        start_day = parseInt(v.start_day);
                                        end_day = parseInt(v.end_day);
                                        time = parseInt(time);
                                        start_time = parseInt(v.start_time_mil);
                                        end_time = parseInt(v.end_time_mil);
                                        start_month = Date.parse(v.start_month);
                                        last_month = Date.parse(v.last_month);
                                        today = parseInt(v.today);
                                        border = adjustCardColors(v.background);
                                        event = `<div class="event" data-id="` + v.id + `" style="color:` + v.color + `;background-color:` + v.background + `; border-left:3px solid ` + border + `;">
                                                    <p>` + v.title + ` - ` + v.description + ` <strong>` + v.start_time + ' - ' + v.end_time + `</strong></p>
                                                </div>`;

                                        if (start_month == last_month) {
                                            start_day = 1;
                                        }

                                        if (today >= start_day && today <= end_day || today == start_day) {
                                            if (time >= start_time && time <= end_time) {
                                                this_cell.find('.events').append(event);
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
        }

        return this.each(function() {
            switch (settings.view) {
                case 'day': action.drawDayView(); break;
                case 'week': action.drawWeekView(); break;
                default: action.drawMonthView(); break;
            }

            action.navigate();
            action.events();
            action.delete();
        });
    }
}(jQuery));

    function adjustColor(col, amt) {
        var use_pound = false;

        if (col[0] == "#") {
            col = col.slice(1);
            use_pound = true;
        }

        var num = parseInt(col, 16);
        var r = (num >> 16) + amt;

        if (r > 255) r = 255;
        else if (r < 0) r = 0;

        var b = ((num >> 8) & 0x00FF) + amt;

        if (b > 255) b = 255;
        else if (b < 0) b = 0;

        var g = (num & 0x0000FF) + amt;

        if (g > 255) g = 255;
        else if (g < 0) g = 0;

        return (use_pound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);
    }

    function colorIsDark(color) {
        var r, g, b, hsp;

        color = + ("0x" + color.slice(1).replace(color.length < 5 && /./g, '$&$&'));
        r = color >> 16;
        g = color >> 8 & 255;
        b = color & 255;

        hsp = Math.sqrt(0.299 * (r * r) + 0.587 * (g * g) + 0.114 * (b * b));

        if (hsp > 127.5) {
            // console.log('light');
            return false;
        } else {
            // console.log('dark');
            return true;
        }
    }
    
    function adjustCardColors(bkg) {

        if (colorIsDark(bkg)) {
            var ammount = 70;
        } else {
            var ammount = -70;
        }
        
        return adjustColor(bkg, ammount);

    }