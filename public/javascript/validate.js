
/**
 * Live form validation
 *
 * The validator plugin validates user forms. It checks basic inputs like 
 * email and passwords. Using the options you can choose to check only
 * the inputs you want by setting the option to true.
 *
 * Example
 *     $('#contact-form').validate({
 *         button : '#login-button',
 *         url : '/login/ajax'
 *     }); 
 */
(function($) {
    $.fn.validate = function(options) {

        var settings = $.extend({
            form: $(this),
            url: null,
            button: '.btn',
            pw_name: 'password',
            conf_name: 'confirm',
            val_pw_str: true,
            required_check: true
        }, options);

        var action = {
            
            validate: function(input) {
                if (settings.url !== null) {
                    $.post(settings.url, input.serialize(), function(response) { 
                        if ($.trim(response)) {
                            var json = JSON.parse(response);
                            var name;
                            var value;
                            if (json.errors) {
                                $.each(json.errors, function(k, v) {
                                    if (input.prop('name') == k) {
                                        name = k;
                                        value = v;
                                    }
                                });
                                if (!input.next().hasClass('alert-attached')) {
                                    $('input[name="' + name + '"]').addClass('error').after('<div class="alert-attached">' + value + '</div>');
                                }  
                            }
                        }
                    });
                }
            },

            validatePassword: function() {
                var password = $('input[name="' + settings.pw_name + '"]');
                var confirm = $('input[name="' + settings.conf_name + '"]');
                if (password.length > 0) {
                    $.post(settings.url, $('input[type="password"]').serialize(), function(response) { 
                        if ($.trim(response)) {
                            var json = JSON.parse(response);
                            if (json.errors) {
                                $.each(json.errors, function(k, v) {
                                    if (confirm.prop('name') == k) {
                                        name = k;
                                        value = v;
                                    }
                                });
                                if (!confirm.next().hasClass('alert-attached')) {
                                    $('input[name="' + name + '"]').addClass('error').after('<div class="alert-attached mb-15">' + value + '</div>');
                                }  
                            }
                        } else {
                            confirm.removeClass('error');
                            confirm.parent().find('.alert-attached').remove();
                        }
                    });
                    if (settings.val_pw_str) {
                        if (password.length > 0 && password.val().length > 0) {
                            if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(password.val()) == false) {
                                $('.password-strength').remove();
                                password.after('<strong class="password-strength">WEAK</strong>');
                                $('.password-strength').css({'color':'#ff6868'});
                            }
                            if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(password.val()) == true) {
                                $('.password-strength').remove();
                                password.after('<strong class="password-strength">STRONG</strong>');
                                $('.password-strength').css({'color':'#5ac952'});
                            }
                            if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{13,}$/.test(password.val()) == true) {
                                $('.password-strength').remove();
                                password.after('<strong class="password-strength">VERY STRONG</strong>');
                                $('.password-strength').css({'color':'#5aa055'})
                            }
                        } else {
                            $('.password-strength').remove();
                        }
                    }
                    
                }
            },

            revealPassword: function() {
                var password = $('input[name="' + settings.pw_name + '"], input[name="' + settings.conf_name + '"]');

                $('.fa-eye').toggleClass('fa-eye-slash');

                if (password.prop('type') == 'password') {
                    password.prop('type', 'text');
                } else if (password.prop('type') == 'text') {
                    password.prop('type', 'password');
                }
            },

            checkRequiredFields: function() {
                if (settings.required) {
                    setTimeout(function() {     
                        var complete = true;
                        var inputs = $(settings.form).find(':input'); // Should return all input elements in that specific form.
                        $('[required]').each(function() {
                            if (!$(this).prop('disabled')) {
                                if ($(this).val() == '') {
                                    complete = false;
                                }
                            }
                        });
                        inputs.each(function() {
                            if ($(this).hasClass('error')) {
                                complete = false;
                            }
                        });
                        if (complete == true) $(settings.button).prop('disabled', false);
                        if (complete == false) $(settings.button).prop('disabled', true);
                    }, 100);
                }
            }
        }
        
        return this.each(function() {
            $('input[name="' + settings.pw_name + '"]').after('<div class="show-pw"><i class="fas fa-eye fa-fw" title="Show password"></i></div>');

            $('.show-pw').on('mousedown', function() {
                action.revealPassword();
            });

            $(document).on('blur', ':input', function() {
                if ($(this).prop('type') !== 'password') {
                    action.validate($(this));
                }
                if ($(this).prop('type') == 'password') {
                    action.validatePassword($(this));
                }
                action.checkRequiredFields();
                if ($(this).val() == '') {
                    $(this).removeClass('error');
                    if ($(this).next().hasClass('alert-attached')) {
                        $(this).parent().find('.alert-attached').remove();
                    }
                }
            });

            $(document).on('click', ':input', function() {
                $(this).removeClass('error');
                if ($(this).next().hasClass('alert-attached')) {
                    $(this).parent().find('.alert-attached').remove();
                }
            });
        });
    }
}(jQuery));