/**
 * Common Javascript 
 *
 * Common js used site wide. Most of this will be used on every page because
 * a lot of it is for the header and navigation stuff. Dropdown menus, to 
 * top buttons, ect.
 */
var Common = new Object();

$(window).on('load', function() {
    Object.keys(Common).forEach(function(key) {
        var func = Common[key];
        if (typeof func === 'function' && key !== 'removeLoader') {
            try {
                func();
            } catch (error) {
                $('body').prepend(error.stack + '\n');
                window.onerror = function(error, file, line, col) {
                    $('body').prepend(error + ' File: ' + file + ' Line: ' + line + ' Col: ' + col + '\n');
                    return false;
                };
            }
        }
    });
}); 

$(window).on('load', function() {
    $('body').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
        Common.removeLoader();
    });
});

// Drop menus
Common.dropMenusDown = function() {
    $(document).on('click', '.dropdown-button', function() {
        $(this).find('.dropdown-menu').toggleClass('active');

        if ($(this).find('.dropdown-menu').hasClass('active')) {
            $(this).find('.dd-toggle').addClass('fa-flip-vertical');
        } else {
            $(this).find('.dd-toggle').removeClass('fa-flip-vertical');
        }

        
        if (!$('.dropdown-menu').hasClass('active') && $('.dropdown-menu').find('.current').hasClass('current')) {
            setTimeout(function() {
                $('.dropdown-menu').css({'position' : 'absolute', 'top' : '50%'});
            }, 150);
        } else {
            $('.dropdown-menu').removeAttr('style');
        }
        
        
    });
}

Common.mobileMenuClick = function() {
    $('body').on('click', '.btn-mobile-menu', function() {
        $('.nav').toggleClass('open');
    });  
}

// Go to top button
Common.goToTop = function() {
    $('.main').append('<div class="to-top"><i class="fas fa-angle-up"></i> Scroll Up</div>');
    $(window).scroll(function() {
        if ($(this).scrollTop() > 520) {
            $('.to-top').addClass('visible');
        } else {
            $('.to-top').removeClass('visible');
        }

        if (!$('.nav').hasClass('wide')) {
            $('.to-top').css({'width' : 65 + 'px', 'overflow' : 'hidden'});
            $('.to-top i').css({'width' : 65 + 'px'});
        } else {
            $('.to-top').removeAttr('style');
            $('.to-top i').removeAttr('style');
        }
    });
    $('.to-top').on('click', function() {
        $('html, body').animate( {
            scrollTop: 0
        }, 300);
    });
}

// Hightlight nav link
Common.highlightNavLink = function() {
    if ($('ul').hasClass('main-nav')) {
        var path = window.location.pathname.split('/');
        var nav_class = path[1] ? '.nav-link.' + path[1] : '.nav-link.dashboard';

        if (path[2]) {
            var nav_class = '.nav-link.' + path[2];
        }
        
        $(nav_class).addClass('current');

        if ($('.dropdown-menu').find('.current').hasClass('current')) {
            $('.dropdown-menu').find('.current').parent().parent().addClass('active');     
            $('.dropdown-menu').find('.current').parent().parent().prev().find('.dd-toggle').addClass('fa-flip-vertical');
        }
    }
}

Common.showLoader = function(param) {
    $(param).prepend('<div class="loading"><div class="loading-spinner"><i class="fas fa-circle-notch fa-fw fa-spin"></i></div></div>');
}

Common.removeLoader = function() {
    $('.loading').remove();
}

Common.goBack = function() {
    var breadcrumb = window.location.pathname.split('/');
    breadcrumb.pop();
    
    if (breadcrumb && breadcrumb.length == 1) {
        breadcrumb[0] = '/';
    }

    var back = breadcrumb.join('/');

    $('.btn-goback').prop('href', back);
}

Common.changeMenuSetting = function() {
    if ($(window).width() > 1024) {
        $('.main-nav').on('click', '.btn-menu', function() {

            if ($(this).parent().parent().parent().hasClass('wide')) {
                var menu_status = 0;
            } else {
                var menu_status = 1;
            }

            $.post('/settings/changeMenuSetting', {'menu_status' : menu_status}, function(response) {
                if ($.trim(response)) {
                    var json = parseInt(JSON.parse(response));
                    if (json == 0) {
                        $('.btn-menu i').removeClass('fa-toggle-on').addClass('fa-toggle-off');
                        $('.nav').removeClass('wide');
                    }
                    if (json == 1) {
                        $('.btn-menu i').addClass('fa-toggle-on').removeClass('fa-toggle-off');
                        $('.nav').addClass('wide');
                    }
                }
            });  
            
            return false;
        });
    }
}