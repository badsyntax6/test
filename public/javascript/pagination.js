/**
 * Pagination
 */
var Pagination = new Object();

/**
 * Pagination Initializer
 *
 * This is the primary funciton for this object. It will set the properties and call all other functions.
 */
Pagination.init = function(url) {
    Pagination.url = url;
    p = {};

    Pagination.refreshTable();
    Pagination.drawPagination();
    Pagination.drawTotalRecords();
    Pagination.drawPageOf();
    Pagination.goToPage();
    Pagination.goPrev();
    Pagination.goNext();
    Pagination.filterBy();
    Pagination.filter();
    Pagination.sortRecords();
    Pagination.checkAll();
    Pagination.enableControls();
    Pagination.highlightRow();
    Pagination.adjustForMobile();
}

/**
 * Refresh Table
 * 
 * This function is responsible for drawing a table of records in the list view. This function will also 
 * call drawPagination() and drawTotalRecords() and numberRows().
 */
Pagination.refreshTable = function() {
    $.post(Pagination.url, p, function(response) {
        if ($.trim(response)) {
            var json = JSON.parse(response);
            
            p.page = json.page;
            p.total_pages = json.total_pages;
            p.total_records = json.total_records;
            p.record_limit = json.record_limit;

            $('.pagination-table').html(json.list);

            Pagination.numberRows(json.start);
            Pagination.drawPagination();
            Pagination.drawTotalRecords();
            Pagination.drawPageOf();
            Pagination.adjustForMobile();
            Pagination.limitPageRecords();
        }
    });
},

Pagination.numberRows = function(start) {
    $('.data-row').each(function(index, item) {
        var start_num = start;
        var num = parseInt(index) + parseInt(start_num);
        num++;
        $(this).find('.number-col').text(num);               
    });
},

/**
 * Draw Pagination
 * 
 * This function will create pagination links for the table list.
 */
Pagination.drawPagination = function() {
    $('.pagination-select option').remove();

    // Add previous button if page is not first page.
    if (p.page == 1) {
        $('.pagination .btn-prev').attr('disabled', true);
    } else {
        $('.pagination .btn-prev').attr('disabled', false);
    }
    // Add page links with page numbers.
    for (var i = 1; i <= p.total_pages; i++) {
        $('.pagination-select').append('<option value="' + i + '">' + i + '</option>');
    }
    // Add next button if page is not last page.
    if (p.page == p.total_pages) {
        $('.pagination .btn-next').attr('disabled', true);
    } else {
        $('.pagination .btn-next').attr('disabled', false);
    }
    // Highlight current page in pagination nav.
    $('.pagination-select option').each(function() {
        if ($(this).val() == p.page) {
            $('.pagination-select').val(p.page);
        }
    });
}

/**
 * Get Total Records
 * 
 * This function will get the total number of records and display the total at the bottom of the list.
 */
Pagination.drawTotalRecords = function() {
    $('.total').text('Total: ' + p.total_records);
}

/**
 * Get Total Records
 * 
 * This function will get the total number of records and display the total at the bottom of the list.
 */
Pagination.drawPageOf = function() {
    $('.page-of').text('Page: ' + p.page + ' of ' + p.total_pages);
}

/**
 * Go to Page
 *
 * This function will re-draw the table starting at the page that is selected in the pagination. 
 */
Pagination.goToPage = function() {
    $('.pagination-select').on('change', function() {
        p.page = $(this).val();
        Pagination.refreshTable();
    });
}

/**
 * Go Previous Page
 * 
 * This function will re-draw the table one page down from the current page.
 */
Pagination.goPrev = function() {
    $('body').on('click', '.btn-prev', function() {
        p.page = --p.page;
        Pagination.refreshTable();
    });
}

/**
 * Go Next Page
 * 
 * This function will re-draw the table one page up from the current page.
 */
Pagination.goNext = function() {
    $('body').on('click', '.btn-next', function() {
        p.page = ++p.page;
        Pagination.refreshTable();
    });
}

/**
 * Limit Page Records
 * 
 * This function will limit the amount of records that are displayed in the table list.
 */
Pagination.limitPageRecords = function() {
    // Add selected prop to current page limit in select menu
    $('.limit-per-page').val(p.record_limit);

    // Choose max records to show per page.
    $('.limit-per-page').on('change', function() {
        p.page = 1;
        p.record_limit = $('option:selected', this).text();
        $('.limit-per-page').val(p.record_limit);
        Pagination.refreshTable();
    });
}

/**
 * Filter Page Records
 * 
 * This function will filter what records are displayed in the table list.
 */
Pagination.filterBy = function() {

    $('.filter-by').on('change', function() {
        $('.filter-dropdown').css({'display' : 'none'});

        if ($('option:selected', this).val()) {
            p.column = $('option:selected', this).val();

            console.log(p.column);
            
            $('[data-filter="' + p.column + '"]').css({'display' : 'block'});
        } else {
            $('.filter-dropdown').css({'display' : 'none'});
            p.column = '';
            Pagination.refreshTable();
        }

        $('.filter-by').val(p.column);
    });
}

/**
 * Filter Page Records
 * 
 * This function will filter what records are displayed in the table list.
 */
Pagination.filter = function() {
    $('.filter').on('change', function() {
        if ($('option:selected', this).val()) {
            p.is = $('option:selected', this).val();
            p.page = 1;
            Pagination.refreshTable();
        } else {
            p.is = '';
            Pagination.refreshTable();
        }

        $('.filter').val(p.is);
    });
}

/**
 * Sort Records
 * 
 * This function sorts the table when the user clicks a link in the table header.
 */
Pagination.sortRecords = function() {
    $('body').on('click', '.btn-sort', function() {
        // Set the order by the table header text.
        p.orderby = $(this).text().toLowerCase().replace(/ /g, '_');

        // Determine the direction the table should sort by.
        if (p.direction === 'asc') {
            p.direction = 'desc';
        } else {
            p.direction = 'asc';
        }
        Pagination.refreshTable();
    });
}

/**
 * Check All
 * 
 * Check all checkboxes if the #check-all checkbox is checked.
 */
Pagination.checkAll = function() {
    $('body').on('click', '#check-all', function() {
        if ($('#check-all').prop('checked') == true) {
            $('.checkbox').prop('checked', true).change();
        }
        if ($('#check-all').prop('checked') == false) {
            $('.checkbox').prop('checked', false).change();
        }
    }); 
}

/**
 * Enable List Controls
 * 
 * This function will enable the master list controls. These buttons will be disabled by default but visible in 
 * the list view. This function listen for a checkbox to change. When it does it will  enable the list controls. 
 * These controls are different from the pagination and limit controls.
 */
Pagination.enableControls = function() {
    $('body').on('change', '.checkbox', function() {
        if ($('input:checkbox:checked').length) {
            $('.btn-list-control').prop('disabled', false);
        }
        if (!$('input:checkbox:checked').length) {
            $('.btn-list-control').prop('disabled', true);
        }
    });  
}

/**
 * Highlight Row
 * 
 * Whenever a row and an element inside a row is clicked this function will highlight that row by changing its 
 * background color.
 */
Pagination.highlightRow = function() {
    $('body').on('click', '.data-row', function() {
        $('tr').removeAttr('style');
        $('.data-row').removeClass('tr-highlight');
        $(this).addClass('tr-highlight');
    });
}

Pagination.adjustForMobile = function() {
    if ($(window).width() <= 414) {

        var th = [];
        var num = 3;
    
        $('th').each(function() {
            th.push($(this).text());
        });
    
        $('.data-row').each(function() {
            $(this).find('.td').each(function() {
                $(this).prepend('<span class="inline-label">' + th[num] + '</span>');
                num = num + 1;
            });
            num = 3;
        });
    } else {
        $('.inline-label').remove();
    }
}