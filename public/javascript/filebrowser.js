/**
 * Filebrowser
 *
 * The filebrowser plugin allows you to browse file on the server.
 * Along the filebrowser php libraries it can create new folders 
 * and with the help of dropzone upload new files.
 * the inputs you want by setting the option to true.
 *
 * Example
 *     $('#elem').filebrowser({
 *         dir : '/root/path/dir/',
 *     }); 
 */
(function($) {

    $.fn.filebrowser = function(options) {

        var settings = $.extend({
            dir : null,
            browser : true,
            add_folders : true
        }, options);

        var action = {

            setCurrentDir: function(dir) {
                localStorage.current_path = null;
                if (localStorage.current_path === 'null') {
                    localStorage.current_path = dir;
                }
            },

            getFiles: function(dir) {

                action.setCurrentDir(dir);

                $('.alert').remove();
                $('.fb-row').remove();
                $('#new-folder-dir').val(dir);
                $('#upload-dir').val(dir);
                $('.fb-title').text(dir);
                $('.filebrowser').append('<div class="loading"><div class="loading-spinner"><i class="fas fa-circle-notch fa-fw fa-spin"></i></div></div>');

                $.post('/filebrowser/browse', {dir : dir}, function(response) {
                    $('.btn-browse').removeClass('disabled').html('<i class="fa fa-folder fa-fw"></i> Browse');
                    $('.btn-close').removeClass('disabled').html('<i class="fa fa-times fa-fw"></i>');
                    if ($.trim(response)) {
                        var json = JSON.parse(response);
                        var dirs = json.dirs;
                        var files = json.files;
                        $(dirs).each(function(k, v) {
                            $('.fb-table').append(
                                `<tr class="fb-row">
                                    <td class="text-center check-cell">
                                        <input type="checkbox" name="fbcheck` + v.name + `" value="` + v.name + `" class="checkbox">
                                    </td>
                                    <td class="fb-item fb-dir"><i class="fas fa-folder fa-fw"></i> 
                                        <a href="#" class="fb-dir-link">` + v.name + `</a>
                                    </td>
                                    <td>`
                                        + v.size +
                                    `</td>
                                    <td>`
                                        + v.modified +
                                    `</td>
                                </tr>`);
                        });
                        $(files).each(function(k, v) {
                            $('.fb-table').append(
                                `<tr class="fb-row">
                                    <td class="check-cell">
                                        <input type="checkbox" name="fbcheck` + v.name + `" value="` + v.name + `" class="checkbox">
                                    </td>
                                    <td class="fb-item fb-file">
                                        <a href="#" class="fb-file-link">` + v.name + `</a>
                                    </td>
                                    <td>`
                                        + v.size +
                                    `</td>
                                    <td>`
                                        + v.modified +
                                    `</td>
                                </tr>`);
                        });
                        $('.fb-file a').each(function() {
                            var filename = $(this).text();
                            var extension = filename.substr((filename.lastIndexOf('.') +1));
                            var ext_ico = action.getExtension(extension);

                            switch (extension) {
                                case 'jpg':
                                case 'JPG':
                                case 'jpeg':
                                case 'JPEG':
                                case 'png':
                                case 'PNG':
                                case 'gif':
                                case 'GIF':
                                    $(this).prepend('<img src="' + localStorage.current_path + 'thumb_' + filename + '" alt="' + filename + '" class="fb-thumbnail">');
                                    break;
                                default:
                                    $(this).prepend(ext_ico + ' ');
                                    break;
                            }
                        });
                        $('.loading').remove();
                    } 
                });
            },

            descend: function(path) {
                var foldername = $(path).text().trim();
                var dir = localStorage.current_path + foldername + '/';
                action.getFiles(dir);
                $('#new-folder-dir').val(dir);
                $('#upload-dir').val(dir + '/');
                $('.fb-btn-back').css({'display' : 'inline-block'});
                dir = dir.split('/');
                dir = dir[dir.length -2];
                $('.filebrowser .title').text('File Browser: ' + dir);
            },

            ascend: function() {
                var current_path = localStorage.current_path;
                var dirs = current_path.split('/');
                var current_dir_name = dirs[dirs.length -2];
                var main_dir = settings.dir;
                $('.filebrowser .title').text('File Browser: ' + current_dir_name);
                dirs = dirs.slice(0, -2);
                dir = dirs.join('/');
                localStorage.current_path = dir + '/';
                action.getFiles(localStorage.current_path);
                $('#new-folder-dir').val(localStorage.current_path);
                $('#upload-dir').val(localStorage.current_path);
                if (localStorage.current_path == main_dir) {
                    $('.fb-btn-back').css({'display' : 'none'});
                }
            },

            makeFolder: function() {
                var dir = $('#new-folder-dir').val();
                $('.filebrowser').append('<div class="loading"><div class="loading-spinner"><i class="fas fa-circle-notch fa-fw fa-spin"></i></div></div>');
                $.post('/filebrowser/makeFolder', $('#new-folder-form').serialize(), function(response) {
                    if ($.trim(response)) {
                        $('.loading').remove();
                        action.getFiles(dir);
                        var json = JSON.parse(response);
                        $('.fb-alert-area').html('<div class="alert ' + json.alert + '"><strong>' + json.alert + '!</strong> ' + json.message + ' <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button>');
                    }
                });
            },

            delete: function() {
                if (confirm('All selected files will be permanently deleted. Are you sure you want to continue?')) {
                    var path = localStorage.current_path;
                    var filenames = $('.checkbox').serializeArray();
                    $.post('/filebrowser/delete', {'filenames' : filenames, 'dir' : path}, function(response) {
                        if ($.trim(response)) {
                            action.getFiles(localStorage.current_path); 
                            var json = JSON.parse(response);
                            $('.fb-alert-area').append('<div class="alert ' + json.alert + '"><strong>' + json.alert + '!</strong> ' + json.message + ' <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button>');
                        }
                    });
                }
            },

            getExtension: function(extension) {
                switch (extension) {                       
                    case 'zip':
                    case 'rar': return '<i class="fas fa-file-archive-o fa-fw"></i>'; break;
                    case 'pdf': return '<i class="far fa-file-pdf fa-fw"></i>'; break;
                    case 'htm':
                    case 'html': return '<i class="fab fa-html5 fa-fw"></i>'; break;
                    case 'php': return '<i class="fas fa-code fa-fw"></i>'; break;
                    case 'css': return '<i class="fas fa-css3 fa-fw"></i>'; break;
                    case 'xlsx': return '<i class="fas fa-file-excel fa-fw"></i>'; break;
                    case 'docx': return '<i class="fas fa-file-word fa-fw"></i>'; break;
                    case 'txt': return '<i class="fas fa-file-word fa-fw"></i>'; break;
                    default: return '<i class="fas fa-question-circle-o fa-fw"></i>';
                }
            }
        }

        return this.each(function() {

            var filebrowser = `<div class="filebrowser">
                                    <div class="fb-titlebar">
                                        <i class="fas fa-folder-open"></i>
                                        <span class="fb-title"></span>
                                        <button type="button"class="btn fb-btn-close no-load pull-right" title="Close"><i class="fas fa-times fa-fw"></i></button>
                                        <button type="button"class="btn fb-btn-refresh no-load pull-right" title="Refresh"><i class="fas fa-sync-alt fa-fw"></i></button>
                                        <button type="button"class="btn fb-btn-back no-load pull-right" title="Back"><i class="fas fa-reply fa-fw"></i></button>
                                    </div>
                                    <div class="fb-fileview">
                                        <div class="fb-actions">
                                            <button type="button" class="btn btn-default fb-btn-upload no-load" title="Upload"><i class="fas fa-upload fa-fw"></i> Upload</button>
                                            <hr>
                                            <form id="new-folder-form">
                                                <input type="hidden" name="new_folder_dir" id="new-folder-dir">
                                                <input type="text" name="foldername" class="fb-new-folder-input col-80" value="New Folder">
                                                <button type="button" class="btn btn-go fb-btn-make-folder btn-attached-left col-20"><i class="fas fa-save fa-fw"></i> Save</button>
                                            </form>
                                            <hr>
                                            <button type="button" class="btn btn-stop fb-btn-mass-delete no-load" title="Delete"><i class="fas fa-trash fa-fw"></i> Delete</button>
                                        </div>
                                        <div class="fb-alert-area"></div>
                                        <table class="fb-table">
                                            <tr>
                                                <th class="check-cell"><input type="checkbox" id="check-all"></th>
                                                <th>Name</th>
                                                <th>Size</th>
                                                <th>Date Modified</th>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="fb-upload">
                                        <button type="button"class="btn fb-btn-close-upload no-load"><i class="fas fa-chevron-down fa-fw"></i></button>
                                        <form class="dropzone needsclick dz-clickable" id="dropzone">
                                            <div class="alert-area"></div>
                                            <input type="hidden" name="upload_dir" id="upload-dir">
                                        </form>
                                    </div>
                                </div>`;

            $(this).append(filebrowser);

            if (settings.browser == false) {
                $('.fb-upload').addClass('open');
                $('.fb-btn-upload').remove();
                $('.fb-btn-close-upload').remove();
            }

            if (settings.add_folders == false) {
                $('.fb-btn-new-folder').remove();
            }

            $('body').on('click', '.btn-browse', function() {
                $('.filebrowser').css({'display': 'block'});
                action.getFiles(settings.dir);
            });

            $('.filebrowser').on('click', '.fb-btn-close', function() { 
                $('.filebrowser').css({'display': 'none'}); 
            });

            $('.filebrowser').on('click', '.fb-btn-refresh', function() { 
                $('.fb-new-folder').removeClass('open');
                action.getFiles(localStorage.current_path); 
            });

            $(document).on('click', '.fb-btn-make-folder', function() {
                action.makeFolder();
            });

            $('#new-folder-form').keypress(function(e) {
                if (e.which == 13) {
                    $('.fb-btn-make-folder').click();
                    return false;
                }
            });

            $('.filebrowser').on('click', '.fb-btn-upload', function() {
                $('.fb-upload').toggleClass('open');
            });

            $('.fb-upload').on('click', '.fb-btn-close-upload', function() {
                $('.fb-upload').removeClass('open');
                $('.dz-preview').remove();
                action.getFiles(localStorage.current_path); 
            });

            $('.filebrowser').on('click', '.fb-dir-link', function(e) {
                e.preventDefault();
                action.descend($(this));
            });

            $('.fb-btn-back').click(function() {
                action.ascend();
            });

            $('.filebrowser').on('click', '.fb-btn-delete', function() {
                var path = localStorage.current_path;
                var parent = $(this).parent().prev().prev().prev().find('a').get(0);
                var file = $(parent).text();
                action.delete(path, file);
            });

            $('body').on('click', '.fb-btn-mass-delete', function() {
                action.delete();
            });

            $('.filebrowser').on('click', '.fb-new-folder-input', function() {
                if ($(this).val() == 'New Folder') $(this).val('');
            });

            $(document).on('click', '#check-all', function() {
                if ($('#check-all').prop('checked') == true) {
                    $('.checkbox').prop('checked', true).change();
                }
                if ($('#check-all').prop('checked') == false) {
                    $('.checkbox').prop('checked', false).change();
                }
            }); 
        });
    }
}(jQuery));