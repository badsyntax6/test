



class App 
{
    options = {};
    constructor(options)
    {
        this.options = options;

        axios.get('/test/json').then(function(response) {
            let data = response.data;
            console.log(data);
        });
    }

    padRows()
    {
        let rows = document.querySelector('main .row').querySelectorAll('.row');
        rows.forEach(function(value, index) {
            value.style.cssText = 'padding: 20px 30px; margin-bottom: 5px; background-color: #ddd;';
        });
    }

    addButton()
    {
        let row3 = document.querySelector('main .row').querySelector('.row:nth-child(3)');
        row3.innerHTML = '<button type="button" class="btn btn-default btn-row3">Row 3 button</button>';
    }

    doButton()
    {
        let btn = document.querySelector('.btn-row3');
        btn.addEventListener('mouseover', function() {
            console.log(this.textContent);
        });
    }

    changeList(btn)
    {
        let li = document.querySelectorAll('li');
        li.forEach(function(v, i) {
            if (v.classList.contains('blue')) {
                v.classList.replace('blue', 'red'); 
            } else {
                v.classList.replace('red', 'blue');  
            }
            
        });
    }

}


const app = new App({
    name: 'chase',
    age: 32,
    food: 'pizza'
});

app.padRows();
app.addButton();
app.doButton();