<?php 

// Define Gusto version.
define('BUILD', '20.02.20-12.10.42');

// Include Gusto config.
require_once str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']) . '/../private/config/settings.php';

// Include start files.
require_once PRIVATE_DIR . '/autoload.php';

Framework::init();