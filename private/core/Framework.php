<?php

/**
 * Core Framework
 * 
 * Where the core basic starter functions and methods live.
 * Instantiated in the index.php
 * 
 * @see root/public/index.php
 */
class Framework
{
    /**
     * Store the single instance of the framework.
     * @property object
     */
    private static $instance;

    /**
     * Framework Construct
     * 
     * Start the framework, include necessary files and instantiate necessary componenent classes.
     * Start the session and handle basic functionality.
     * 
     * @return void
     */
    public function __construct()
    {
        if (!isset($_SESSION)) { session_start(); }
        
        require_once PRIVATE_DIR . '/config/db.php';
        require_once PRIVATE_DIR . '/config/app.php'; 
        require_once PRIVATE_DIR . '/helpers/common.php';

        set_error_handler(function($num, $err, $file, $line) {
            if ($num) {
                $error = $err . ' in ' . $file . ' on line ' . $line . "\n";
                Log::server($error);
                echo $error . '<br>';
            }
        });

        register_shutdown_function(function() {
            $e = error_get_last();
            if ($e['type'] === E_ERROR) Log::server($e['message'] . ' in ' . $e['file'] . ' on line ' . $e['line'] . "\n");
        });

        Application::init();
        Router::init();
    }

    /**
     * Create a static instance of this class.
     * 
     * @return self
     */
    public static function init() 
    {
        if (!isset(static::$instance)) {
            static::$instance = new static();
        }
        return static::$instance;
    }
}