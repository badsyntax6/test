<?php

class Application
{
    /**
     * Store the single instance of the application.
     * @property object
     */
    private static $instance;

    /**
     * Application Construct
     * 
     * Start the application, include necessary files and instantiate necessary componenent classes.
     * Start the session and handle basic functionality.
     * 
     * @return void
     */
    public function __construct()
    {        
           
    }

    /**
     * Create a static instance of this class.
     * 
     * @return self
     */
    public static function init() 
    {
        if (!isset(static::$instance)) {
            static::$instance = new static();
        }
        return static::$instance;
    }
}