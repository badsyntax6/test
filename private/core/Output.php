<?php 

/**
 * Output Core Class
 * 
 * Output different types of data.
 */
class Output
{
    /**
     * Output JSON
     *
     * Take an array and convert to json then tepending on the action exit, return or echo the json.
     * 
     * @param array $array
     * @param boolean $action
     * @return void
     */
    public static function json($array)
    {
        echo json_encode($array);
    }

    /**
     * Output Text
     * 
     * Return plain text.
     *
     * @param string $text
     * @return void
     */
    public static function text($content)
    {
        $content = preg_replace('@<script[^>]*?>.*?</script>@si', '', $content);
        $content = preg_replace('@<style[^>]*?>.*?</style>@si', '', $content);
        $content = strip_tags($content);
        $content = trim($content);
        return $content;
    }

    /**
     * Output HTML
     * 
     * Echo HTML
     *
     * @param string $view
     * @param array $data
     * @return void
     */
    public static function html($view, $data = [])
    {
        if (is_array($data)) extract($data);

        $file = PUBLIC_DIR . '/htm/' . $view . '.htm';
        if (is_file($file)) {
            ob_start();
            require $file;
            echo ob_get_clean();
        }
    }
}