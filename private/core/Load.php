<?php 

/**
 * Load Core Class
 *
 * The loader class will load the various components of the MVC application. 
 * The loading methods will instantiate classes. The loading methods will also 
 * exit with an error if it cannot instantiate the class. This application uses
 * autoloading so includes and requires should not be necessary.
 */
class Load
{   
    /**
     * Load a controller class
     * 
     * Require and instantiate a controller class based on the parameter passed to the method.
     * @example Load::controller('home');
     * 
     * @param string $controller - Controller class name
     * @return object
     */
    public static function controller($controller)
    {   
        $delimiters = ['/', '\\', '-', '_', '.'];

        foreach ($delimiters as $d) {
            if (strpos($controller, $d)) {
                $keys = explode($d, $controller);
                foreach ($keys as $key) {
                    $array[] = ucfirst($key);
                }
                $controller = implode($array);
            }
        }

        $class = ucfirst($controller . 'Controller');

        if (class_exists($class)) $controller = new $class();

        if (get_class($controller)) return $controller;

        exit('The Load class was unable to load the controller ' . $class . '.');
    }

    /**
     * Load a model class
     * 
     * Require and instantiate the model class based on the parameter passed to the method.
     * @example Load::
     * 
     * @param string $model
     * @return object
     */
    public static function model($model)
    {
        $model = ucfirst($model . 'Model');

        if (class_exists($model)) return new $model(); 

        exit('The "' . $model . '" class does not exist.');
    }

    /**
     * Load a view file
     * 
     * Process the view data for display in the view and require the view file. Exit with notification 
     * if file cannot be found or opened. This method returns the view so it doesnt immediatly echo 
     * out the view. This means you can store the view in a variable and use it when needed. This is 
     * different from Output::html() which will echo out the view the moment it is called.
     * 
     * @see root/private/core/Output.php - Output::html()
     * @param string $view 
     * @param array $data 
     * @return string
     */
    public static function view($view, $data = [])
    {
        if (is_array($data)) extract($data);

        $file = PUBLIC_DIR . '/htm/' . $view . '.htm';

        if (is_file($file)) {
            ob_start();
            require $file;
            return ob_get_clean();
        }

        exit('The view file ( ' . PUBLIC_DIR .  '/htm/' . $view . '.htm ) cannot be found.');
    }

    /**
     * Library loader
     * 
     * Require and instantiate the library class based on the parameter passed to the method.
     * 
     * @param string $library
     * @return object
     */
    public static function library($library)
    {
        $library = ucfirst($library);

        if (class_exists($library)) return new $library();

        exit('The "' . $library . '" library class does not exist.');
    }

    /**
     * Route
     *
     * Redirects user to new route.
     * 
     * @param string $route
     */
    public static function route($route)
    {
        exit(header('Location:' . $route));
    }
}