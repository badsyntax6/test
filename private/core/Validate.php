<?php 

/**
 * Validate core class
 * 
 * Class to handle form/input validation so new validation code doesnt need to be written 
 * for every input. The methods in this class are chainable so they can be used together.
 * This method does not give back and error or reason for validation failure because of
 * language support. Errors that display to the user should come from a language file and
 * and be set in the controller. The error in the language file should explain all the 
 * reasons validation may have failed.
 * 
 * @example Basic usage
 * Validate::max(30)->username($_POST['username'])->min(8);
 * 
 * @example Usage with a response
 * if (Validate::max(20)->alphanum($_POST['username])) {
 *     $username = $_POST['username];
 * } else {
 *     $error['username'] = Language::get('signup/username_invalid');
 * }
 * 
 * @example Language file response text
 * Usernames must be letters A-Z and no more than 20 characters long.
 */
class Validate
{   
    /**
     * Input value
     * @property mixed
     */
    public static $input = false;

    /**
     * Instance of this class
     * @property object
     */
    public static $instance = null;

    /**
     * If an input is required this will be set to true
     * @property boolean
     */
    public static $required = false;

    /**
     * Min length for input value
     * @var boolean
     */
    public static $min = false;

    /**
     * Max length for input value
     * @var boolean
     */
    public static $max = false;

    /**
     * Empty becomes true if an input is not filled
     * @var boolean
     */
    public static $empty = false;

    /**
     * Validate email addresses
     *
     * @param string $email
     * @return self
     */
    public static function email($email)
    {      
        self::$input = (filter_var($email, FILTER_VALIDATE_EMAIL));

        return self::result($email);
    }

    /**
     * Validate telephone numbers
     *
     * @param string $num
     * @return self
     */
    public static function tel($num)
    {              
        if ((preg_match("/^[0-9+ ()-]+$/", $num))) self::$input = $num;
        else self::$input = false;
        return self::result($num);
    }

    /**
     * Validate URLs
     *
     * @param string $url
     * @return self
     */
    public static function url($url)
    {
        if (filter_var($url, FILTER_VALIDATE_URL)) self::$input = $url;
        return self::result($url);
    }

    /**
     * Validate words
     * 
     * Words should be a-z uppercase or lowercase and spaces are allowed.
     *
     * @param string $string
     * @return self
     */
    public static function words($string)
    {
        if (preg_match("/^[a-zA-Z ]+$/", $string)) self::$input = $string;
        else self::$input = false;
        return self::result($string);
    }

    /**
     * Validate alphabetical strings
     *
     * Alphabetical string should be a-z uppercase or lowercase with no spaces.
     * 
     * @param string $string
     * @return self
     */
    public static function alpha($string)
    {
        if (preg_match("/^[a-zA-Z]+$/", $string)) self::$input = $string;
        else self::$input = false;
        return self::result($string);
    }

    /**
     * Validate numbers
     *
     * Numbers should not contain any aplphabetical or special characters.
     * This method is not strict so numbers can be int or string. 
     * 
     * @param mixed $num
     * @return self
     */
    public static function num($num)
    {
        if (preg_match("/^[0-9]+$/", $num)) self::$input = $num;
        else self::$input = false;
        return self::result($num);
    }

    /**
     * Validate alphanumeric strings
     *
     * Alphanumeric strings may contain both numbers and letters but no spaces.
     * 
     * @param string $num
     * @return self
     */
    public static function alphaNum($string)
    {
        if (preg_match("/^[a-zA-Z0-9]+$/", $string)) self::$input = $string;
        else self::$input = false;
        return self::result($string);
    }

    /**
     * Validate text
     * 
     * Text is lenient and accepts words, numbers and some special characters.
     *
     * @param string $string
     * @return self
     */
    public static function text($string)
    {
        if (preg_match("/^[a-zA-Z0-9 \-_,.():;!@$&#%?]+$/", $string)) self::$input = $string;
        else self::$input = false;
        return self::result($string);
    }

    /**
     * Validate passwords
     * 
     * Check if a password is weak or strong. A strong password much contain the following...
     * 1) At least 8 characters 
     * 2) At least 1 number 
     * 3) At least 1 lowercase letter 
     * 4) At least 1 uppercase letter 
     * 5) At least 1 special character - @$!%*?&
     * 
     * @param string $input
     * @return self
     */
    public static function password($password)
    {
        if (preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/", $password)) self::$input = $password;
        else self::$input = false;
        return self::result($password);
    }

    /**
     * Validate custom strings
     * 
     * If none of the predefined validation methods are useful and you 
     * need to define your own validation criteria, this method can 
     * be used by passing some custom regex to it as a parameter.
     *
     * @param mixed $input - Input to be validated
     * @param string $regex - A regex pattern to match against the input
     * @return self
     */
    public static function custom($input, $regex)
    {
        if (preg_match("/$regex/", $input)) self::$input = $input;
        else self::$input = false;
        return self::result($input);
    }

    /**
     * Determine the minimum length of an input string
     * 
     * If an input string is too short / does not have the minimum amount
     * of characters in it, the input property will be set to null.
     *
     * @param mixed $min - The minimum amount of characters. Can be int or string.
     * @return void
     */
    public static function min($min)
    {
        self::$min = $min;
        return self::instance();
    }

    /**
     * Determine the maximum length of an input string
     * 
     * If an input string is too long / has too many characters in it, 
     * the input property will be set to null.
     *
     * @param mixed $min - The maximum amount of characters. Can be int or string.
     * @return void
     */
    public static function max($max)
    {
        self::$max = $max;
        return self::instance();
    }

    /**
     * Make input required
     * 
     * If this method is part of the call chain the input
     * will be considered required. So if the input is not set
     * The validator will return false and the input will be 
     * considered invalid.
     *
     * @return void
     */
    public static function required()
    {
        self::$required = true;
        return self::instance();
    }

    /**
     * Get the result of the chainging validation methods
     * 
     * This method should spit out the final result of the validation.
     * If the input failed any part of its validation the chainging methods
     * the self::$input property should be null. If so return false. If the 
     * input passed return it.
     *
     * @return mixed - The input whatever it may be.
     */
    public static function result($input)
    {
        $result = false;

        if (self::nullOrEmpty($input)) self::$empty = true;

        if (!self::$empty && self::$min && strlen(self::$input) < self::$min) {
            return false;
        }

        if (!self::$empty && self::$max && strlen(self::$input) > self::$max) {
            return false;
        }

        if (!self::$input && self::$required) {
            return false;
        }

        if (!self::$required && self::$empty) {
            return true;
        }
        
        $result = self::$input;
        
        self::$min = false;
        self::$max = false;
        self::$required = false;
        self::$input = false;
        self::$empty = false;

        return $result;
    }

    /**
     * Check if the input value is empty or null
     *
     * @param mixed $value
     * @return bool
     */
    public static function nullOrEmpty($value)
    {
        if (is_null($value) || empty($value)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get Validation Instance
     * 
     * Creating and return a single instance of this class.
     * 
     * @return object
     */
    public static function instance()
    {
        if (!isset(static::$instance)) {
            static::$instance = new static();
        }
        return static::$instance;
    }
}