<?php 

/**
 * Model Core Class
 *
 * The model class is the main model of the application system.
 * All model classes will be extensions of this class.
 */
class Model
{
    /**
     * Reference to database object
     * @var object
     */
    private $con;

    /**
     * Table to be used in the query
     * @var string
     */
    private $table;

    /**
     * The query string
     * @var string
     */
    private $query = '';

    /**
     * Post data to be inserted or updated
     * @var mixed
     */
    private $post_data;

    /**
     * Limit of records to get
     * @var int
     */
    private $limit;

    public static $db_hits = 0;


    /**
     * Model construct
     * 
     * @return void
     */
    public function __construct()
    {
        $this->con = Database::instance();
    }

    /**
     * Table
     * 
     * Begins building the query string by creating a table property.
     * Also calls the reset method which sets all class properties back 
     * to null so we can start a fresh query.
     *
     * @param string $table - Table to be used.
     * @return object
     */
    public function table($table)
    {
        $this->reset();

        $this->table = $table;

        return $this;
    }

    /**
     * Reset query properties
     * 
     * Reset the class properties so when a new query is started 
     * fresh data can be manipulated.
     *
     * @return void
     */
    public function reset()
    {
        $this->post_data = null;
        $this->query = null;
        $this->limit = null;
    }

    /**
     * Select
     * 
     * Begins building the select portion of the query string.
     * Multiple columns can be select or you can use "*" everything.
     *
     * @param string $select - What to select.
     * @return object
     */
    public function select($select)
    {
        if (strpos($select, ',')) {
            $choices = explode(', ', $select);
        } else {
            $choices = [$select];
        }

        foreach ($choices as $c) {
            $selects[] = $select == '*' ? $c : '`' . $c .  '`';
        }

        $selects = implode(', ', $selects);

        $this->query = 'SELECT ' . $selects . ' FROM `' . $this->table . '`' . $this->query;

        return $this->get();
    }

    /**
     * Count records
     * 
     * Begins a count query string. Once one of the "get" methods 
     * are used with this query string. You can get the record count
     * from a table. Faster than doing a select and counting afterwards.
     *
     * @return object
     */
    public function count()
    {
        $this->query = 'SELECT COUNT(1) FROM `' . $this->table . '`';

        return $this->get();
    }

    /**
     * Where
     *
     * Begin building the where portion of the query. Also 
     * create a where property to be used later.
     * 
     * @param string $where
     * @param mixed $equals - Probably string or int
     * @return object
     */
    public function where($where, $equals = null)
    {
        if (is_null($equals)) {
            $this->query = $this->query . ' WHERE `' . $where . '` = ?';
        } else {
            $this->query = $this->query . ' WHERE `' . $where . '` = "' . $equals . '"';
        }
        
        return $this;
    }

    /**
     * And where
     * 
     * Continue building the where portion of the query.
     *
     * @param string $where
     * @param mixed $equals - Probably string or int
     * @return object
     */
    public function andWhere($where, $equals = null)
    {
        if (is_null($equals)) {
            $this->query = $this->query . ' AND `' . $where . '` = ?';
        } else {
            $this->query = $this->query . ' AND `' . $where . '` = "' . $equals . '"';
        }
        
        return $this;
    }

    /**
     * Or where
     * 
     * Continue building the where portion of the query.
     *
     * @param string $where
     * @param mixed $equals - Probably string or int
     * @return object
     */
    public function orWhere($where, $equals = null)
    {
        if (is_null($equals)) {
            $this->query = $this->query . ' OR `' . $where . '` = ?';
        } else {
            $this->query = $this->query . ' OR `' . $where . '` = "' . $equals . '"';
        }
        
        return $this;
    }

    /**
     * Where not
     *
     * Begin building the where portion of the query.
     * 
     * @param string $where
     * @param mixed $equals - Probably string or int
     * @return object
     */
    public function whereNot($where, $equals = null)
    {
        if (is_null($equals)) {
            $this->query = $this->query . ' WHERE `' . $where . '` != ?';
        } else {
            $this->query = $this->query . ' WHERE `' . $where . '` != "' . $equals . '"';
        }
        
        return $this;
    }

    /**
     * And where not
     * 
     * Continue building the where portion of the query.
     *
     * @param string $where
     * @param mixed $equals - Probably string or int
     * @return object
     */
    public function andWhereNot($where, $equals = null)
    {
        if (is_null($equals)) {
            $this->query = $this->query . ' AND `' . $where . '` != ?';
        } else {
            $this->query = $this->query . ' AND `' . $where . '` != "' . $equals . '"';
        }
        
        return $this;
    }

    /**
     * Where like
     *
     * Begin building the where portion of the query.
     * 
     * @param string $where
     * @param mixed $like - Probably string or int
     * @return object
     */
    public function whereLike($where, $like)
    {
        $this->query = $this->query . '  WHERE `' . $where . '` LIKE "%'. $like . '%"';

        return $this;
    }

    /**
     * Or where like
     *
     * Continue building the where portion of the query.
     * 
     * @param string $where
     * @param mixed $like - Probably string or int
     * @return object
     */
    public function orWhereLike($where, $like)
    {
        $this->query = $this->query . ' OR `' . $where . '` LIKE "%'. $like . '%"';

        return $this;
    }

    /**
     * Where greater
     *
     * Begin building the where portion of the query.
     * 
     * @param string $record
     * @param mixed $param - Probably string or int
     * @return object
     */
    public function whereGreater($record, $param)
    {
        $this->query = $this->query . ' WHERE `' . $record . '` > ' . $param;

        return $this;
    }

    /**
     * Where lesser
     *
     * Begin building the where portion of the query.
     * 
     * @param string $record
     * @param mixed $param - Probably string or int
     * @return object
     */
    public function whereLesser($record, $param)
    {
        $this->query = $this->query . ' WHERE `' . $record . '` < ' . $param;

        return $this;
    }

    /**
     * Where highest
     *
     * Begin building the where portion of the query.
     * 
     * @param string $record
     * @param mixed $param - Probably string or int
     * @return object
     */
    public function highest($record)
    {
        $this->query = $this->query . ' WHERE `' . $record . '` = (SELECT max(' . $record . ') FROM `' . $this->table . '`)';

        return $this;
    }

    /**
     * Where highest and where
     * 
     * Begin building the where portion of the query.
     *
     * @param string $record
     * @param string $column
     * @param mixed $is - Probably string or int
     * @return object
     */
    public function highestAndWhere($record, $column, $is)
    {
        $this->query = $this->query . ' WHERE `' . $record . '` = (SELECT max(' . $record . ') FROM `' . $this->table . '` WHERE `' . $column . '` = "' . $is . '")';

        return $this;
    }

    /**
     * Where month
     *
     * Begin building the where month portion of the query.
     * 
     * @param string $column
     * @param string $month
     * @param string $year
     * @return object
     */
    public function whereMonth($column, $month, $year)
    {
        $this->query = $this->query . ' WHERE MONTH(`' . $column . '`) = ' . $month . ' AND YEAR(`' . $column . '`) = ' . $year;

        return $this;
    }

    /**
     * Where date
     *
     * Begin building the where date portion of the query.
     * 
     * @param string $column
     * @param string $day
     * @param string $month
     * @param string $year
     * @return object
     */
    public function whereDate($column, $day, $month, $year)
    {
        $this->query = $this->query . ' WHERE DAY(`' . $column . '`) = ' . $day . ' AND MONTH(`' . $column . '`) = ' . $month . ' AND YEAR(`' . $column . '`) = ' . $year;

        return $this;
    }
    
    /**
     * Order by
     * 
     * Continue building the query string deciding which order the 
     * records will come in.
     *
     * @param string $orderby
     * @param string $direction
     * @return object
     */
    public function orderBy($orderby, $direction)
    {
        $this->query = $this->query . ' ORDER BY `' . $orderby . '` ' . strtoupper($direction);

        return $this;
    }

    /**
     * Limit
     * 
     * Continue building the query string and limiting the amount
     * of records.
     *
     * @param int $limit
     * @return object
     */
    public function limit($limit)
    {
        $this->query = $this->query . ' LIMIT ' . $limit;
        $this->limit = $limit;

        return $this;
    }

    /**
     * Limit between
     * 
     * Continue building the query string and limiting the amount
     * of records.
     *
     * @param int $start
     * @param int $end
     * @return object
     */
    public function limitBetween($start, $end)
    {
        $this->query = $this->query . ' LIMIT ' . $start . ', ' . $end;

        return $this;
    }

    /**
     * Left join
     * 
     * Begin a join portion of the query string, joinging 2 tables.
     * Returns all records from the left table, and the matched 
     * records from the right table.
     *
     * @param string $join - A table to join
     * @param string $selector1 - A column to compare to another
     * @param string $selector2 - A column to be compared against
     * @return object
     */
    public function leftJoin($join, $selector1, $selector2)
    {
        $this->query = $this->query . ' LEFT JOIN `' . $join . '` ON ' . $this->table . '.' . $selector1 . ' = ' . $join . '.' . $selector2;

        return $this;
    }

    /**
     * Right join
     * 
     * Begin a join portion of the query string, joinging 2 tables.
     * Returns all records from the right table, and the matched 
     * records from the left table.
     *
     * @param string $join - A table to join
     * @param string $selector1 - A column to compare to another
     * @param string $selector2 - A column to be compared against
     * @return object
     */
    public function rightJoin($join, $selector1, $selector2)
    {
        $this->query = $this->query . ' RIGHT JOIN `' . $join . '` ON ' . $this->table . '.' . $selector1 . ' = ' . $join . '.' . $selector2;

        return $this;
    }

    /**
     * Inner join
     * 
     * Begin a join portion of the query string, joinging 2 tables.
     * Returns records that have matching values in both tables.
     *
     * @param string $join - A table to join
     * @param string $selector1 - A column to compare to another
     * @param string $selector2 - A column to be compared against
     * @return object
     */
    public function innerJoin($join, $selector1, $selector2)
    {
        $this->query = $this->query . ' INNER JOIN `' . $join . '` ON ' . $this->table . '.' . $selector1 . ' = ' . $join . '.' . $selector2;

        return $this;
    }

    /**
     * Full join
     * 
     * Begin a join portion of the query string, joinging 2 tables.
     * Returns all records when there is a match in either left or right table.
     *
     * @param string $join - A table to join
     * @param string $selector1 - A column to compare to another
     * @param string $selector2 - A column to be compared against
     * @return object
     */
    public function fullJoin($join, $selector1, $selector2)
    {
        $this->query = $this->query . ' FULL JOIN `' . $join . '` ON ' . $this->table . '.' . $selector1 . ' = ' . $join . '.' . $selector2;

        return $this;
    }

    /**
     * Delete
     * 
     * Begin the delete portion of the query.
     *
     * @return object
     */
    public function delete()
    {
        $this->query = 'DELETE FROM `' . $this->table . '`' . $this->query;

        return $this->execute();
    }

    /**
     * Truncate
     * 
     * Begin the truncate portion of the query.
     *
     * @return object
     */
    public function truncate($log)
    {
        $this->query = 'TRUNCATE TABLE `' . $log . '`';

        return $this->con->mysqli->query($this->query);
    }

    /**
     * Get
     *
     * Using the query string, execute the fetch query and return an array
     * with a response string.
     * 
     * @param string $type - The type of fetch to do.
     * @return array
     */
    public function get()
    {
        Model::$db_hits++;
        
        $response = [];

        if ($query = $this->con->mysqli->query($this->query)) {
            
            while ($col = $query->fetch_assoc()) {
                $response[] = $col;
            }

            if (isset($response[0]) && array_key_exists('COUNT(1)', $response[0])) {
                $response = implode(array_values($response[0]));
            }
            
            if ($this->limit == 1) {
                if (!empty($response)) {
                    $response = $response[0];
                }

                if (is_array($response) && count($response) == 1) {
                    $response = isset($response[0]) ? $response[0] : $response;
                    if (isset($response) && count($response) == 1) {
                        $response = implode($response);
                    }                
                }
            }

            return ['status' => 'success', 'response' => $response];
        }

        return $this->debug($this->con->mysqli->error);
    }

    /**
     * Insert
     * 
     * Using the query string prepare the data for insertion into the database.
     *
     * @param mixed $data - Array of data to be inserted.
     * @return self
     */
    public function insert($data)
    {
        foreach (array_keys($data) as $k) {
            $values[] = '`' . $k .  '`';
            $placeholders[] = '?';
        }

        $values = implode(', ', $values);
        $placeholders = implode(', ', $placeholders);

        $this->post_data = $data;
        $this->query = 'INSERT INTO `' . $this->table . '` (' . $values . ') VALUES (' . $placeholders . ')';

        return $this->execute();
    }

    /**
     * Update
     *
     * Using the query string prepare the data to be updated in the database.
     * 
     * @param mixed $data - Array of data to be updated.
     * @return self
     */
    public function update($data)
    {        
        foreach (array_keys($data) as $k) {
            $values[] = '`' . $k .  '`';
        }

        $values = implode(' = ?, ', $values);

        $this->post_data = $data;
        $this->query = 'UPDATE `' . $this->table . '` SET ' . $values . ' = ?' . $this->query;

        return $this->execute();
    }

    /**
     * Execute
     * 
     * After the data has been prepared by either the update or the insert
     * methods execute the query.
     *
     * @return array
     */
    public function execute()
    {
        Model::$db_hits++;
        if (isset($this->post_data)) {
            // Query params need to be in the same order as post data. It's also necessary to remove the WHERE/AND/OR params from the SET part of the query.
            if (preg_match_all('/(WHERE `\w+`|AND `\w+`|OR `\w+`)/', $this->query, $matches)) {
                foreach ($matches[0] as $m) {
                    $arr[] = preg_replace('/\w.* `(\w+)`/', '$1', $m);
                }
        
                $cons = array_unique($arr);

                foreach ($cons as $c) {   
                    if (strpos($this->query, ', `' . $c . '` = ?')) {
                        $this->query = str_replace(', `' . $c . '` = ?', '', $this->query);
                    }
                    if (strpos($this->query, '`' . $c . '` = ?,')) {
                        $this->query = str_replace('`' . $c . '` = ?,', '', $this->query);
                    }

                    // Move WHERE/AND/OR params to the end of the post data array.
                    $this->post_data += array_splice($this->post_data, array_search($c, array_keys($this->post_data)), 1); 
                }
            }

            $params = substr_count($this->query, '?');

            for ($i = 0; $i < $params; $i++) { 
                $types[] = 's';
            }

            if ($query = $this->con->mysqli->prepare($this->query)) {
                if ($query->bind_param(implode($types), ...array_values($this->post_data))) {
                    if ($query->execute()) {
                        return ['status' => 'success', 'affected_rows' => $query->affected_rows];
                    } else {
                        $this->debug($this->con->mysqli->error);
                        return ['status' => 'error', 'response' => $this->con->mysqli->error];
                    }
                } else {
                    $this->debug($this->con->mysqli->error);
                }
            } else {
                $this->debug($this->con->mysqli->error);
            }    

            return;
        }
        
        if ($this->con->mysqli->query($this->query)) {
            return ['status' => 'success', 'response' => ''];
        } else {
            $this->debug($this->con->mysqli->error);
        }   
    }
    
    /**
     * Test select and/or execute
     * 
     * Echo and var dump a select query.
     *
     * @return void
     */
    public function test()
    {
        echo '<pre>', var_dump($this->query), '</pre>';
        if (isset($this->post_data)) echo '<pre>' , var_dump($this->post_data) , '</pre>';
    }

    /**
     * Debug
     * 
     * If there's an error in the sql query log it and determine where it came from.
     *
     * @see $this->log()
     * @param string $sql_error - The error returned by sql.
     * @return array
     */
    public function debug($sql_error)
    {
        $backtrace = debug_backtrace();
        $data = $backtrace[3];

        $data['file'] = explode('\\', $data['file']);
        $data['file'] = end($data['file']);

        $controller = str_replace('.php', '', $data['file']);
        $string = ' ERROR: ' . $data['class'] . '->' . $data['function'] . ' - ' . $sql_error . '. - Called by ' . $controller . ' on line ' . $data['line'] . '.';
        
        Log::db($string);
    }
}