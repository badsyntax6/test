<?php 

/**
 * Language Core Class
 * 
 * This language library will be loaded in the application controller. So you can 
 * always use the language library in your controller classes.
 * @example Language::get('filename/key');
 */
class Language
{
    /**
     * Get language setting
     * 
     * Get the language setting from the settings table and return it.
     *
     * @return string 
     */
    public static function getLanguageSetting()
    {
        $query = Database::instance()->mysqli->query('SELECT `value` FROM `settings` WHERE `name` = "language"');
        $lang = $query->fetch_object();  
        return $lang->value;
    }

    /**
     * Return language value
     * 
     * The language library includes the language files and returns values from them. 
     * The parameter passed to the get method should be a string with the filename 
     * and index seperated by a forward slash.
     * @example Language::get('home/title');
     * 
     * @param string $path - The path to the language file and index. file/index = home/title
     * @return string
     */
    public static function get($path, $vars = null)
    {
        $language = self::getLanguageSetting();
        $path = explode('/', $path);
        $file = isset($path[0]) ? $path[0] : 'home';
        $index = isset($path[1]) ? $path[1] : '';

        if (is_file(LANGUAGE_DIR . '/' . $language . '/' . $file . '.php')) {
            include LANGUAGE_DIR . '/' . $language . '/' . $file . '.php';
        } else {
            include LANGUAGE_DIR . '/' . $language . '/dashboard.php';
        }

        if (isset($_[$index])) {
            $result = isset($vars) ? self::replaceVarPlaceholders($_[$index], $vars) : $_[$index];
            return $result;
        } else {
            return 'Error: Language key (' . $index . ') not found in (' . $file . ').';
        }
    }

    /**
     * Replace placeholders in language strings
     * 
     * Replace the placeholders in the language string with their
     * counterparts in the vars array.
     *
     * @param string $string
     * @param array $vars - Variables to replace placeholders
     * @return string
     */
    public static function replaceVarPlaceholders($string, $vars)
    {
        foreach ($vars as $key => $value) {
            $string = str_replace('{{' . $key . '}}', $value, $string);
        }

        return $string;
    }
}