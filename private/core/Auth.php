<?php

/**
 * Authentication Class
 * 
 * Various methods to check user authentication and privilege. Authentication and
 * routing seem to be tightly coupled so this class extends the Router Class.
 * Perhaps this could change in the future idk.
 * 
 * @see Route Core Class - /root/private/core/Router.php
 */
class Auth extends Router
{
    public static $user = null;

    /**
     * Check if user is logged in
     * 
     * Check if the user id session is set. If the session is set access
     * the user model user the session user id and get the user data 
     * related to the session.
     * 
     * @return array
     */
    public static function isLogged()
    {
        // Check if the session is set.
        if (isset($_SESSION['id'])) { 
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if the route requires a login
     * 
     * Getting the routes access level from the Router(parent) class we can
     * determine if the route requires a login. Anything greater than 0
     * requires a login.
     *
     * @return bool
     */
    public static function requiresLogin()
    {
        if (parent::$route_level > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the access/route level
     * 
     * Get the current route level so we can check what level the 
     * user needs to be to access the route.
     *
     * @return int
     */
    public static function accessLevel()
    {
        return parent::$route_level;
    }

    /**
     * Get the logged users data
     * 
     * If a user is logged in get all the users data from the database so 
     * its available in the mvc. All user data is then accessible via this
     * class. 
     * 
     * @example Auth::user()->firstname
     * @example shorter way Auth::firstname()
     * @return object
     */
    public static function user()
    {
        if (!self::isLogged()) return;

        if (is_null(self::$user)) {
            self::$user = (object)Load::model('user')->getUser('key', Session::get('id'));
        }

        return self::$user;
    }

    /**
     * Get the logged users id
     *
     * @example Auth::id()
     * @return string
     */
    public static function id()
    {
        if (self::user()) return self::user()->user_id;        
    }

    /**
     * Get the logged users key
     *
     * @example Auth::key()
     * @return string - complex randomized string unique to each user
     */
    public static function key()
    {
        if (self::user()) return self::user()->key;
    }

    /**
     * Get the logged users group
     *
     * @example Auth::group()
     * @return string - The users group or access level. Unregistered users are 1 admins are 4.
     */
    public static function group()
    {
        if (self::user()) return self::user()->group;
    }

    /**
     * Get the logged users firstname
     *
     * @example Auth::firstname()
     * @return string
     */
    public static function firstname()
    {
        if (self::user()) return self::user()->firstname;
    }

    /**
     * Get the logged users lastname
     *
     * @example Auth::lastname()
     * @return string
     */
    public static function lastname()
    {
        if (self::user()) return self::user()->lastname;
    }

    /**
     * Get the logged users full name
     *
     * @example Auth::fullname()
     * @return string
     */
    public static function fullname()
    {
        if (self::user()) return self::user()->firstname . ' ' . self::user()->lastname;
    }

    /**
     * Get the logged users username
     *
     * @example Auth::username()
     * @return string
     */
    public static function username()
    {
        if (self::user()) return self::user()->username;
    }

    /**
     * Get the logged users email address
     *
     * @example Auth::email()
     * @return string
     */
    public static function email()
    {
        if (self::user()) return self::user()->email;
    }
}