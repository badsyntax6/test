<?php 

/**
 * Router class
 * 
 * The Router class will examine the url, and compare it to routes in the routing
 * arrays. If a route is found, the user will be dispatched to the route.
 */
class Router
{
    /**
     * Controller class
     * @property object
     */
    private static $controller = 'DashboardController';

    /**
     * Controller method
     * @property string
     */
    private static $action = 'index';

    /**
     * Controller parameters
     * @property array
     */
    private static $params = [];

    /**
     * Route access level
     * @property int
     */
    protected static $route_level;

    /**
     * Create an array of routes
     * 
     * These are the default routes for Gusto. They should theoretically never change.
     * All custom routes that are made when making a new app should go in the
     * routes.php file. This method will include the routes.php file and merge the
     * custom routes from that files array, into this methods array. The routes 
     * include the level of access required to visit the route.
     * 
     * @example Levels
     * 
     * 0 = Un-registered / locked
     * 1 = Registered but not yet activated
     * 2 = Registered and activated
     * 3 = Moderator
     * 4 = Admin
     *
     * @see root/private/routes.php
     * @return array
     */
    private static function routes()
    {
        $routes = [
            // SIGNUP
            ["route" => "signup",                           "controller" => "SignupController",           "action" => "index",                  "level" => 0],
            ["route" => "signup/validate",                  "controller" => "SignupController",           "action" => "validate",               "level" => 0],
            ["route" => "signup/validate-live",             "controller" => "SignupController",           "action" => "validateLive",           "level" => 0],
            // LOGIN
            ["route" => "login",                            "controller" => "LoginController",            "action" => "index",                  "level" => 0],
            ["route" => "login/validate",                   "controller" => "LoginController",            "action" => "validate",               "level" => 0],
            ["route" => "login/validate-live",              "controller" => "LoginController",            "action" => "validateLive",           "level" => 0],
            // LOGOUT
            ["route" => "logout",                           "controller" => "LogoutController",           "action" => "index",                  "level" => 0],
            ["route" => "logout/destroySession",            "controller" => "LogoutController",           "action" => "destroySession",         "level" => 0],
            ["route" => "logout/inactive",                  "controller" => "LogoutController",           "action" => "inactive",               "level" => 0],
            ["route" => "logout/confirm",                   "controller" => "LogoutController",           "action" => "confirm",                "level" => 0],
            // HOME/DASHBOARD
            ["route" => "/",                                "controller" => "DashboardController",        "action" => "index",                  "level" => 2],
            ["route" => "dashboard",                        "controller" => "DashboardController",        "action" => "index",                  "level" => 2],
            ["route" => "dashboard/clear-log",              "controller" => "DashboardController",        "action" => "clearLog",               "level" => 4],
            ["route" => "dashboard/clear-errors",           "controller" => "DashboardController",        "action" => "clearErrors",            "level" => 4],
            // SEARCH
            ["route" => "search",                           "controller" => "SearchController",           "action" => "index",                  "level" => 2],
            // HTTP STATUS
            ["route" => "status/{\w+}",                     "controller" => "StatusController",           "action" => "index",                  "level" => 0],
            // ACCOUNT
            ["route" => "account",                          "controller" => "AccountController",          "action" => "index",                  "level" => 2],
            ["route" => "account/getAccountJson",           "controller" => "AccountController",          "action" => "getAccountJson",         "level" => 2],
            ["route" => "account/uploadAvatar",             "controller" => "AccountController",          "action" => "uploadAvatar",           "level" => 2],
            ["route" => "account/validate",                 "controller" => "AccountController",          "action" => "validate",               "level" => 0],
            ["route" => "account/activate",                 "controller" => "AccountController",          "action" => "activate",               "level" => 0],
            ["route" => "account/send",                     "controller" => "AccountController",          "action" => "send",                   "level" => 0],
            ["route" => "account/forgot",                   "controller" => "AccountController",          "action" => "forgot",                 "level" => 0],
            ["route" => "account/send-reset-request",       "controller" => "AccountController",          "action" => "sendResetRequest",       "level" => 0],
            ["route" => "account/reset/{\w+}",              "controller" => "AccountController",          "action" => "reset",                  "level" => 0],
            ["route" => "account/save-new-password",        "controller" => "AccountController",          "action" => "saveNewPassword",        "level" => 0],
            ["route" => "account/validate-live",            "controller" => "AccountController",          "action" => "validateLive",           "level" => 0],
            ["route" => "account/changeThemeSetting",       "controller" => "AccountController",          "action" => "changeThemeSetting",     "level" => 2],
            // SETTINGS
            ["route" => "settings",                         "controller" => "SettingsController",         "action" => "index",                  "level" => 4],
            ["route" => "settings/update/{\w+}",            "controller" => "SettingsController",         "action" => "update",                 "level" => 4],
            ["route" => "settings/general",                 "controller" => "SettingsController",         "action" => "general",                "level" => 4],
            ["route" => "settings/mail",                    "controller" => "SettingsController",         "action" => "mail",                   "level" => 4],
            ["route" => "settings/server",                  "controller" => "SettingsController",         "action" => "server",                 "level" => 4],
            ["route" => "settings/getSettingsJson",         "controller" => "SettingsController",         "action" => "getSettingsJson",        "level" => 4],
            ["route" => "settings/getInactivityLimit",      "controller" => "SettingsController",         "action" => "getInactivityLimit",     "level" => 2],
            ["route" => "settings/updateLastActive",        "controller" => "SettingsController",         "action" => "updateLastActive",       "level" => 2],
            ["route" => "settings/getMenuSetting",          "controller" => "SettingsController",         "action" => "getMenuSetting",         "level" => 2],
            ["route" => "settings/changeMenuSetting",       "controller" => "SettingsController",         "action" => "changeMenuSetting",      "level" => 2],
            ["route" => "settings/updateMailSettings",      "controller" => "SettingsController",         "action" => "updateMailSettings",     "level" => 4],
            ["route" => "settings/test-email",              "controller" => "SettingsController",         "action" => "testEmail",              "level" => 4],
            // USERS
            ["route" => "users",                            "controller" => "UsersController",            "action" => "index",                  "level" => 2],
            ["route" => "users/user/{[a-z\-]+}",            "controller" => "UsersController",            "action" => "user",                   "level" => 2],
            ["route" => "users/getTable",                   "controller" => "UsersController",            "action" => "getTable",               "level" => 2],
            ["route" => "users/edit",                       "controller" => "UsersController",            "action" => "edit",                   "level" => 4],
            ["route" => "users/delete",                     "controller" => "UsersController",            "action" => "delete",                 "level" => 4],
        ];
        
        $custom_routes = self::getCustomRoutes();
        $routes = array_merge($routes, $custom_routes);

        return $routes;
    }

    /**
     * Initiate the Router class
     * 
     * Get all the routes and compare them against the url.
     * If a match is found, set the controller and action by using the route
     * array values. The leftover parts of the matched url if any, become parameters.
     * Controller, action and parameters are sent to the dispatch method as parameters.
     * If no match is found, dispatch is called with the 404/StatusController.
     * 
     * @see self::routes()
     * @return void
     */
    public static function init()
    {
        foreach (self::routes() as $r) {     
            $url = isset($_GET['url']) ? filter_var(trim($_GET['url'], '/'), FILTER_SANITIZE_URL) : '/';
            $route = preg_replace("/\//", "\\/", $r['route']);
            $route = preg_replace('/\{([^\}]+)\}/', '(\1)', $route);

            if (preg_match("/^$route$/", $url, $matches)) {
                $params = isset($matches[1]) ? array_filter(explode('/', $matches[1])) : [];
                self::$route_level = $r['level'];
                return self::dispatch($r['controller'], $r['action'], $params);
            }
        } 

        return self::dispatch('StatusController', 'index', ['404']);
    }

    /**
     * Distpach user to route
     * 
     * Using the controller, instantiate the controller class, and using 
     * the action string, call the action method. $params will be used 
     * as parameters for the action/method. 
     * @example $controller->$action($params)
     * 
     * @see self::init()
     * @param string $controller - Controller class name
     * @param string $action - Controller method name
     * @param array $params - Parameters for the controller method
     * @return void
     */
    private static function dispatch($controller, $action, $params = [])
    {
        if (!class_exists($controller)) $controller = 'StatusController';

        self::$controller = new $controller();

        if (!method_exists(self::$controller, $action)) array_unshift($params, $action);
        else self::$action = $action;

        self::$params = array_values($params);
        
        if (is_callable([self::$controller, self::$action])) {
            if (method_exists(self::$controller, 'beforeFilter')) self::$controller->beforeFilter();
            call_user_func_array([self::$controller, self::$action], self::$params);
            if (method_exists(self::$controller, 'afterFilter')) self::$controller->afterFilter();
        }

        // echo Model::$db_hits;
    }

    /**
     * Get the custom routes array
     * 
     * Include the routes.php file. Which is an array just like the routes 
     * array, in this classes routes() method.
     * 
     * @see self::routes()
     * @see root/private/routes.php
     * @return array
     */
    private static function getCustomRoutes()
    {
        return require_once PRIVATE_DIR . '/routes.php';
    }
}
