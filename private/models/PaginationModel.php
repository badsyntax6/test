<?php 

/**
* Pagination Model
*/
class PaginationModel extends Model
{
    /**
     * Paginate records.
     *
     * @param string $table
     * @param string $orderby
     * @param string $direction
     * @param int $start
     * @param int $limit
     * @param string $where
     * @return array
     */
    public function paginate($table = null, $orderby = null, $direction = null, $start = null, $limit = null, $column = null, $is = null)
    {
        if (isset($start) && isset($limit)) {
            $orderby = $this->checkOrderby($orderby, $table);
            $total = $this->getTotalRecordsNumber($table, $column, $is);
            $total = $total ? $total : 0;
            $pages = ceil($total / $limit);
            $start = ($start-1) * $limit;
            $records = $this->getRecords($table, $orderby, $direction, $start, $limit, $column, $is);
            $records = $records ? $records : [];
            $output = ['pages' => $pages, 'start' => $start, 'records' => $records, 'total' => $total];
            return $output;
        }
    }

    /** 
     * Get total number of records from a given table.
     *
     * @param string $table
     * @return mixed - return an array or false
     */
    public function getTotalRecordsNumber($table, $column, $is)
    {
        if ($column && $is) {
            $select = $this->table($table)->where($column, $is)->count();
        } else {
            $select = $this->table($table)->count();
        }
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    /**
     * Get some records
     *
     * @return mixed - return an array or false
     */
    public function getRecords($table, $orderby, $direction, $start, $limit, $column, $is)
    {
        if ($column && $is) {
            $select = $this->table($table)->where($column, $is)->orderBy($orderby, $direction)->limitBetween($start, $limit)->select('*');
        } else {
            $select = $this->table($table)->orderBy($orderby, $direction)->limitBetween($start, $limit)->select('*');
        }
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? [] : $select['response'];
            } else {
                return false;
            }
        }
    }

    /**
     * Check the order by parameters
     * 
     * Users click on table headers to sort paginated tables.
     * The table header text is not always the same as the column so the switch
     * in this method will convert the text.
     *
     * @param string $orderby - What to order by
     * @param string $table - What table we are sorting through
     * @return void
     */
    public function checkOrderby($orderby, $table)
    {
        switch ($orderby) {
            case 'status':
                if ($table == 'users') return 'last_active';
                else return $orderby;
            break;
            case 'name': return 'firstname'; break;
            default: return $orderby; break;
        }
    }
}