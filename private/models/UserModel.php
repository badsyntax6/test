<?php 

/**
 * User Model Class
 *
 * Interact with the database to process data related to the users.
 */
class UserModel extends Model
{
    /**
     * Get all user data
     *
     * Get all users data and return their data in arrays.
     * @return array
     */
    public function getUsers()
    {
        $select = $this->table('user')->select('*');
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    /**
     * Get a single users data
     *
     * Get a users data and return their data in an array.
     * @param $param
     * @param $data  
     * @return array
     */
    public function getUser($param, $data)
    {
        $select = $this->table('user')->leftJoin('menus', 'user_id', 'menu_anchor')->where($param, $data)->limit(1)->select('*');
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function getUserProfile($firstname, $lastname)
    {
        $select = $this->table('user')->where('firstname', $firstname)->andWhere('lastname', $lastname)->limit(1)->select('*');
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    /**
     * Insert user into database
     *
     * Insert a new user record into the database.
     * @param array $post      
     * @return bool   
     */
    public function insertUser($post)
    {
        $insert = $this->table('user')->insert($post);
        if ($insert) {
            if ($insert['status'] == 'success') {
                return empty($insert['response']) ? true : $insert['response'];
            } else {
                return false;
            }
        }
    }

    /**
     * Update a user record
     *
     * @param mixed $data   
     * @return mixed         
     */
    public function updateUser($data, $is)
    {
        $update = $this->table('user')->where($is)->update($data);
        if ($update) {
            if ($update['status'] == 'success') {
                if ($update['affected_rows'] > 0) {
                    return empty($update['response']) ? true : $update['response'];
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public function getCountries()
    {
        $select = $this->table('countries')->select('*');
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? true : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function getLoginAttempts($is, $email)
    {
        $select = $this->table('logins')->where('ip', $is)->andWhere('email', $email)->limit(1)->select('*');
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function insertLoginAttempt($data)
    {
        $insert = $this->table('logins')->insert($data);
        if ($insert) {
            if ($insert['status'] == 'success') {
                return empty($insert['response']) ? true : $insert['response'];
            } else {
                return false;
            }
        }
    }

    public function updateLoginAttempts($data)
    {
        $update = $this->table('logins')->where('email')->andWhere('ip')->update($data);
        if ($update) {
            if ($update['status'] == 'success') {
                if ($update['affected_rows'] > 0) {
                    return empty($update['response']) ? true : $update['response'];
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public function deleteLoginAttempts($ip)
    {
        $delete = $this->table('logins')->where('ip', $ip)->delete();
        if ($delete) {
            if ($delete['status'] == 'success') {
                return empty($delete[1]) ? true : $delete[1];
            } else {
                return false;
            }
        }
    }

    public function getUsersNumber($data = null)
    {
        if (is_null($data)) {
            $select = $this->table('user')->count();
        } else {
            $select = $this->table('user')->where('group', $data)->count();
        }
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function deleteUser($id)
    {
        $delete = $this->table('user')->where('user_id', $id)->delete();
        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserMenuSetting($is)
    {
        $select = $this->table('menus')->where('menu_anchor', $is)->limit(1)->select('main_menu');   
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function getUserThemeSetting($is)
    {
        $select = $this->table('user')->where('user_id', $is)->limit(1)->select('theme');   
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    /**
     * Update a user record
     *
     * @param mixed $data   
     * @return mixed         
     */
    public function updateUserMenuSetting($data)
    {
        $update = $this->table('menus')->where('menu_anchor')->update($data);
        if ($update) {
            if ($update['status'] == 'success') {
                if ($update['affected_rows'] > 0) {
                    return empty($update['response']) ? true : $update['response'];
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * Insert user into database
     *
     * Insert a new user record into the database.
     * @param array $post      
     * @return bool   
     */
    public function createUserMenuSetting($user_id)
    {
        $data['menu_anchor'] = $user_id;
        $data['main_menu'] = 0;
        $insert = $this->table('menus')->insert($data);
        if ($insert) {
            if ($insert['status'] == 'success') {
                return empty($insert['response']) ? true : $insert['response'];
            } else {
                return false;
            }
        }
    }
}