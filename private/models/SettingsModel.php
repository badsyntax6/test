<?php 

/**
 * Settings Model Class
 *
 * Interact with the database to process data related to the settings.
 */
class SettingsModel extends Model
{
    /**
     * Get all settings data
     *
     * Get all settingss data and return their data in arrays.
     * @return array
     */
    public function getSettings()
    {
        $select = $this->table('settings')->select('*');
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function getSetting($data)
    {
        $select = $this->table('settings')->where('name', $data)->limit(1)->select('value');
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function updateSetting($data)
    {
        $update = $this->table('settings')->where('setting_id')->update($data);
        if ($update) {
            if ($update['status'] == 'success') {
                return empty($update['response']) ? true : $update['response'];
            } else {
                return false;
            }
        }
    }

    public function getMailSettings()
    {
        $select = $this->table('mail')->limit(1)->select('*');
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    public function insertMailSettings($data)
    {
        $insert = $this->table('mail')->insert($data);
        if ($insert) {
            if ($insert['status'] == 'success') {
                return empty($insert['response']) ? true : $insert['response'];
            } else {
                return false;
            }
        }
    }

    public function updateMailSettings($data)
    {
        $update = $this->table('mail')->where('mail_id')->update($data);
        if ($update) {
            if ($update['status'] == 'success') {
                return empty($update['response']) ? true : $update['response'];
            } else {
                return false;
            }
        }
    }

    public function getLanguages()
    {
        $select = $this->table('language')->select('*');
        if ($select) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }
}