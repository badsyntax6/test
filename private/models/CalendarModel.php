<?php

class CalendarModel extends Model
{
    /**
     * Insert a calendar event
     *
     * @param array $post - Post data
     * @return bool
     */
    public function insertEvent($post)
    {
        if ($insert = $this->table('calendar')->insert($post)) {
            if ($insert['status'] == 'success') {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Update a calendar event
     *
     * @param array $data - Post data
     * @param string $where - Value to update by
     * @return bool
     */
    public function updateEvent($data, $where)
    {
        if ($update = $this->table('calendar')->where($where)->update($data)) {
            if ($update['status'] == 'success') {
                if ($update['affected_rows'] > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * Delete a calendar event
     *
     * @param string $id - Event id
     * @return bool
     */
    public function deleteEvent($id)
    {
        if ($delete = $this->table('calendar')->where('event_id', $id)->delete()) {
            if ($delete['status'] == 'success') {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Get all calendar events
     *
     * @param string $month - Should be a month and year. @example 2019-09
     * @return mixed If there is data return it, if not return false
     */
    public function getEvents($month)
    {
        $m = date('m', strtotime($month));
        $y = date('Y', strtotime($month));

        if ($select = $this->table('calendar')->whereMonth('start_time', $m, $y)->select('event_id, title, description, start_time, end_time, color, background')) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }

    /**
     * Get a single calendar event
     *
     * @param string $column - Column where desired data is.
     * @param string $is - Desired data name.
     * @return mixed If there is data return it, if not return false
     */
    public function getEvent($column, $is)
    {
        if ($select = $this->table('calendar')->where($column, $is)->limit(1)->select('*')) {
            if ($select['status'] == 'success') {
                return empty($select['response']) ? false : $select['response'];
            } else {
                return false;
            }
        }
    }
}
