<?php 

/**
 * File Browser
 *
 * The file browser library will interface with the file system to browse,
 * create and delete files and folders. It relies on the filebrowser
 * controller and the filbrowser js file to take input from the user.
 * 
 * @see Filebrowser Controller Class - /root/private/controllers/FilebrowserController.php
 * @see Filebrowser JS - /root/public/javascript/filebrowser.php
 */
class FileBrowser
{
    public $system_file = false;
    public $file_deleted = false;
    public $directory_deleted = false;

    /**
     * Browse the file system
     * 
     * Browse for files and folders. Sort them and return them in an array.
     *
     * @return array
     */
    public function browse()
    {
        $dirs = [];
        $files = [];
        $ddata = [];
        $fdata = [];

        if (!empty($_POST['dir'])) $dir = PUBLIC_DIR . $_POST['dir'];

        // $dir = PUBLIC_DIR . '/uploads/foo/';

        $found = array_diff(scandir($dir), ['.', '..']);

        foreach ($found as $f) {
            if (is_dir($dir . $f)) $dirs[] = $f;
            if (is_file($dir . $f)) $files[] = $f;
        }

        natcasesort($dirs);
        natcasesort($files);

        $exclude = ['index.htm', '.htaccess'];
        $dirs = array_values($dirs);
        $files = array_values(array_diff($files, $exclude));
        $files = array_filter($files, function($string) {
            return strpos($string, 'thumb_') === false;
        });

        foreach ($dirs as $d) {
            $ddata[] = [
                'name' => $d,
                'size' => formatBytes(getDirSize($dir . $d)),
                'modified' => date('d-m-Y g:ia', filemtime($dir . $d))
            ];
        }

        foreach ($files as $f) {
            $fdata[] = [
                'name' => $f,
                'size' => formatBytes(filesize($dir . $f)),
                'modified' => date('d-m-Y g:ia', filemtime($dir . $f))
            ];
        }

        $found = ['dirs' => $ddata, 'files' => $fdata];

        return $found;
    }

    /**
     * Create a new folder
     *
     * @return void
     */
    public function makeFolder($foldername, $directory)
    {
        if (is_dir($directory . $foldername)) return false;
        if (mkdir($directory . $foldername)) {
            $forbidden = fopen($directory . $foldername . '/' . 'index.htm', 'w') or exit('Unable to open file!');
            fwrite($forbidden, "<!DOCTYPE html>\n<html>\n<head>\n    <title>403 Forbidden</title>\n</head>\n<body>\n    <p>Directory access is forbidden.</p>\n</body>\n</html>");
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete a file or directory
     *
     * @return bool
     */
    public function delete($directory, $file) 
    {
        if ($file === 'default_blog.jpg' || $file === 'index.htm') $this->system_file = true;
        if (is_file($directory . $file)) {            
            unlink($directory . $file);
            if (file_exists($directory . 'thumb_' . $file)) unlink($directory . 'thumb_' . $file);
            $this->file_deleted = true;
        }
        if (is_dir($directory . $file)) {
            $directory = $directory . $file;
            $it = new RecursiveDirectoryIterator($directory, RecursiveDirectoryIterator::SKIP_DOTS);
            $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);

            foreach ($files as $file) {
                if ($file->isDir()) {
                    rmdir($file->getRealPath());
                } else {
                    unlink($file->getRealPath());
                }
            }

            rmdir($directory);
            
            $this->directory_deleted = true;
        }

        return $this;
    }
}