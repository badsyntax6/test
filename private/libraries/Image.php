<?php 

/**
 * Image Library Class
 *
 * This library is used to process images that have been uploaded.
 */
class Image
{   
    /**
     * This method targets an image in a given directory and makes adjustments to it.
     * Good for if you need to change the width, height or crop an image on the server.
     * 
     * @param string $source - Source image.
     * @param integer $width - Desired width for new image.
     * @param integer $height - Desired height for new image.
     * @param string $filename - Desired filename for new image.
     * @param string $directory -Directory for new image.
     * 
     * @return boolean
     */
    public function cropImage($source = '', $width = 60, $height = 60, $filename = '', $directory = '')
    {
        $type = mime_content_type($source); // Determine the file type of the source image.
        $image = $this->createImage($source);

        list($source_width, $source_height) = getimagesize($source); // Get the width and height from the source image.

        if ($source_width > $source_height) { // Do some math I don't understand. Thanks stack overflow. Pretty sure it crops the image proportionately.
            $new_height = $height;
            $new_width = floor($source_width * ($new_height / $source_height));
            $crop_x = ceil(($source_width - $source_height) / 2);
            $crop_y = 0;
        } else {
            $new_width = $width;
            $new_height = floor($source_height * ($new_width / $source_width));
            $crop_x = 0;
            $crop_y = ceil(($source_height - $source_width) / 2);
        }

        $tmp = imagecreatetruecolor($width, $height); // Create a temporary image

        if ($type == 'image/gif' || $type == 'image/png') {
            imagecolortransparent($tmp, imagecolorallocatealpha($tmp, 0, 0, 0, 127));
            imagealphablending($tmp, false);
            imagesavealpha($tmp, true);
        }
        
        imagecopyresampled($tmp, $image, 0, 0, $crop_x, $crop_y, $new_width, $new_height, $source_width, $source_height); // Copy and resize part of the new image with resampling.

        $this->saveImage($type, $tmp, $directory . '/', $filename);

        imagedestroy($image);
        imagedestroy($tmp);

        return true; 
    }

    /**
     * Make a thumbnail for and uploaded image
     *
     * @param string $source
     * @param string $directory
     * @param int $desired_width
     * @return void
     */
    public function makeThumbnail($source, $directory, $desired_width)
    {
        $type = mime_content_type($source);
        $path = pathinfo($source);
        $filename = $path['basename'];
        $source_image = $this->createImage($source);
        $width = imagesx($source_image);
        $height = imagesy($source_image);
        $desired_height = floor($height * ($desired_width / $width));
        $tmp = imagecreatetruecolor($desired_width, $desired_height);

        if ($type == 'image/gif' || $type == 'image/png') {
            imagecolortransparent($tmp, imagecolorallocatealpha($tmp, 0, 0, 0, 127));
            imagealphablending($tmp, false);
            imagesavealpha($tmp, true);
        }
        
        imagecopyresampled($tmp, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

        $this->saveImage($type, $tmp, $directory . '/', 'thumb_' . $filename);
    }

    /**
     * Create an image
     * 
     * Create an image resource from an image file. This image does not exist
     * on disk yet. The image needs to be saved using the $this->saveImage() method.
     *
     * @see $this->saveImage()
     * @param string $source
     * @return string
     */
    private function createImage($source)
    {
        $type = mime_content_type($source); // Determine the file type of the source image.

        if ($type == 'image/jpg' || $type == 'image/jpeg') $image = imagecreatefromjpeg($source);
        if ($type == 'image/png') $image = imagecreatefrompng($source);
        if ($type == 'image/gif') $image = imagecreatefromgif($source);

        return $image;
    }

    /**
     * Save the image
     * 
     * This method actually saves the image to the disk.
     *
     * @param string $type
     * @param string $tmp
     * @param string $directory
     * @param string $filename
     * @return void
     */
    private function saveImage($type, $tmp, $directory, $filename)
    {
        if ($type == 'image/gif' || $type == 'image/png') {
            header('Content-Type: image/png');
            imagepng($tmp, $directory . $filename, 0); 
        } else {
            header('Content-Type: image/jpeg');
            imagejpeg($tmp, $directory . $filename, 100);  
        }
    }
}