<?php 

/**
 * Upload Library Class
 *
 * This class is will be used to validate and upload files to the server.
 */
class Upload
{
    /**
     * Name of the file to upload.
     * @var string
     */
    public $filename;

    /**
     * Temporary name of the file to upload.
     * @var string
     */
    public $filetmp;

    /**
     * Type of the file to upload.
     * @var string
     */
    public $filetype;

    /**
     * Size of the file to upload.
     * @var int
     */
    public $filesize;

    /**
     * Error if the selected file is invalid.
     * @var string
     */
    public $file_error;

    /**
     * Array of the filename to upload.
     * @var array
     */
    public $tmp;

    /**
     * Extension of the file to upload.
     * @var string
     */
    public $extention;

    /**
     * Alert if the file to upload has an invalid extension.
     * @var boolean
     */
    public $file_invalid = false;

    /**
     * Alert if the file to upload is too large.
     * @var boolean
     */
    public $file_big = false;

    /**
     * Alert if the upload is successful or not.
     * @var boolean
     */
    public $upload_success = false;

    public $file_exists = false;

    /**
     * Upload an Image
     *
     * @param array $source - Source image
     * @param string $directory - Directory to upload to
     * @param integer $size - Max size of the image
     * @return void
     */
    public function uploadImage($source, $directory, $size = 1000000)
    {
        $extensions = ['jpg', 'jpeg', 'png', 'gif', 'tif', 'tiff', 'svg'];
        $max_size = $size;

        $this->process($source);
        $this->validate($extensions, $max_size, str_replace('//', '/', $directory));
        if ($this->move($directory)) {
            $this->upload_success = [
                'name' => $this->filename, 
                'type' => $this->filetype, 
                'size' => $this->filesize, 
                'ext' => $this->extention, 
                'dir' => str_replace('//', '/', PUBLIC_DIR . $directory),
                'source' => str_replace('//', '/', PUBLIC_DIR . $directory . $this->filename)
            ];
        }
    }

    /**
     * Upload a document
     *
     * @param array $source - Source document
     * @param string $directory - Directory to upload to
     * @param integer $size - Max size of the document
     * @return void
     */
    public function uploadDoc($source, $directory, $size = 1000000)
    {
        $extensions = ['txt', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'xml'];
        $max_size = $size;

        $this->process($source);
        $this->validate($extensions, $max_size, $directory);
        if ($this->move($directory)) {
            $this->upload_success = [
                'name' => $this->filename, 
                'type' => $this->filetype, 
                'size' => $this->filesize, 
                'ext' => $this->extention, 
                'dir' => str_replace('//', '/', PUBLIC_DIR . $directory),
                'source' => str_replace('//', '/', PUBLIC_DIR . $directory . $this->filename)
            ];
        }
    }

    /**
     * Upload a document
     *
     * @param array $source - Source document
     * @param string $directory - Directory to upload to
     * @param integer $size - Max size of the document
     * @return void
     */
    public function uploadAnything($source, $directory, $size = 1000000)
    {
        $extensions = ['txt', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'xml', 'jpg', 'jpeg', 'png', 'gif', 'tif', 'tiff', 'svg'];
        $max_size = $size;

        $this->process($source);
        $this->validate($extensions, $max_size, $directory);
        if ($this->move($directory)) {
            $this->upload_success = [
                'name' => $this->filename, 
                'type' => $this->filetype, 
                'size' => $this->filesize, 
                'ext' => $this->extention, 
                'dir' => str_replace('//', '/', PUBLIC_DIR . $directory),
                'source' => str_replace('//', '/', PUBLIC_DIR . $directory . $this->filename)
            ];
        }
    }

    /**
     * Validate file
     *
     * @param array $extensions - Allowed extensions
     * @param int $max_size - Max file size of the file
     * @return void
     */
    public function validate($extensions, $max_size, $directory) 
    {
        if (in_array($this->extention, $extensions) === false) $this->file_invalid = true;       
        if ($this->filesize > $max_size) $this->file_big = true;

        $found = array_diff(scandir(PUBLIC_DIR . $directory), ['.', '..']);

        foreach ($found as $f) {
            if ($f == $this->filename) $this->file_exists = true;
        }

        $this->filesize = formatBytes($this->filesize);
        $this->max_size = formatBytes($max_size);
    }

    /**
     * Process the file for upload
     * 
     * Ensure the file name is uniques and that it does not contain malicious characters.
     *
     * @param string $source - Source file
     * @return void
     */
    public function process($source)
    {
        $this->filename = trim(strtolower(str_replace(' ', '_', $source['name'])));
        $this->filename = preg_replace('/^[^a-z0-9._-]+$/', '', $this->filename);
        $this->filetmp = $source['tmp_name'];
        $this->filetype = $source['type'];
        $this->filesize = $source['size'];
        $this->file_error = $source['error'];
        $this->tmp = explode('.', $this->filename);
        $this->extention = strtolower(end($this->tmp));
    }

    /**
     * Move the file
     * 
     * Move the temporary uploaded file to a permanent location
     *
     * @param string $directory - Directory to store file
     * @return void
     */
    public function move($directory)
    {
        if (!$this->file_invalid && !$this->file_big) {
            if (move_uploaded_file($this->filetmp, PUBLIC_DIR . $directory . $this->filename)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function makeThumbnail($source, $directory, $desired_width)
    {
        // Get path info
        $path = pathinfo($source);
        // Save the new path using the current file name
        $filename = $path['basename'];
        // Do the rest of your stuff and things...
        $source_image = imagecreatefromjpeg($source);
        $width = imagesx($source_image);
        $height = imagesy($source_image);
        $desired_height = floor($height * ($desired_width / $width));
        $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
        
        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
        imagejpeg($virtual_image, $filename);
        rename(PUBLIC_DIR . '/' . $filename, $directory . 'thumb_' . $filename);
    }
}