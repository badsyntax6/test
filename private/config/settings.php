<?php 

// Directories
define('PUBLIC_DIR', str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']));
define('PRIVATE_DIR', str_replace('/public', '/private', PUBLIC_DIR));
define('HOST', isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' . $_SERVER['HTTP_HOST'] : 'http://' . $_SERVER['HTTP_HOST']);
define('CONTROLLERS_DIR', PRIVATE_DIR . '/controllers');
define('CORES_DIR', PRIVATE_DIR . '/core');
define('LANGUAGE_DIR', PRIVATE_DIR . '/language');
define('LIBRARIES_DIR', PRIVATE_DIR . '/libraries');
define('MODELS_DIR', PRIVATE_DIR . '/models');
define('VENDORS_DIR', PRIVATE_DIR . '/vendors');
define('STORAGE_DIR', PRIVATE_DIR . '/storage');


// Timezone
date_default_timezone_set('Pacific/Honolulu');

// Errors
error_reporting(E_ALL); 
ini_set('display_errors', 'On');

// Prevent site from being loaded by iframes.
header('X-Frame-Options: SAMEORIGIN'); // DENY or SAMEORIGIN