<?php 

/**
 * Title
 */
$_['title'] = 'Test';

/**
 * Description
 */
$_['description'] = 'This is the test page description and it is about 160 characters long, which is super important for seo or (search engine optimization). Try to keep it so.';

/**
 * Alerts
 */
$_['foo'] = 'Hello my name is {{name}} and I am {{age}}. I like to eat {{food}}.';