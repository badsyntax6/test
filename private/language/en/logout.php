<?php 

/**
 * Title
 */
$_['title'] = 'Logout';

/**
 * Description
 */
$_['description'] = 'This is the logout page description and it is about 160 characters long, which is super important for seo or (search engine optimization). Try to keep it so.';

/**
 * Alerts
 */

 
/**
 * Logs
 */
$_['log_logged_out'] = 'User <b><i>{{name}}</i></b> logged out.';
$_['log_inactive_logout'] = 'User <b><i>{{name}}</i></b> was logged out due to inactivity.';