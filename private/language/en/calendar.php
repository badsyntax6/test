<?php

/**
 * Title
 */
$_['title'] = 'Calendar';

/**
 * Description
 */
$_['description'] = '';

/**
 * Alerts
 */
$_['event_saved'] = 'The new calendar event was saved.';
$_['event_saved_fail'] = 'The new calendar event could not be saved';
$_['event_updated'] = 'The new calendar event was updated.';
$_['event_update_fail'] = 'The new calendar event could not be updated';