<?php 

/**
 * Title
 */
$_['title'] = 'Account';

/**
 * Description
 */
$_['description'] = 'No bio information at this time.';

/**
 * Alerts
 */
$_['account_updated'] = 'Your account has been updated.';
$_['username_taken'] = 'The username you have chosen is already taken.';
$_['username_invalid'] = 'Usernames must be letters A-Z and no more than 20 characters long.';
$_['username_fail'] = 'Unable to update username.';
$_['firstname_invalid'] = 'First name should be letters a-z and no spaces.';
$_['lastname_invalid'] = 'Last name should be letters a-z and no spaces.';
$_['phone_invalid'] = 'Phone numbers should only be numbers 0-9.';
$_['email_taken'] = 'The email address you have chosen is already taken.';
$_['email_invalid'] = 'The email address you entered was invalid.';
$_['password_weak'] = 'The password is too weak. <br><b>Passwords must:</b> <br>- Have at least 1 number. <br>- Have at least 1 upper case letter. <br>- Be 8 or more characters. (<a href="">Try Again?</a>)';
$_['birthday_invalid'] = 'The birthday you entered appears to be invalid.';
$_['country_invalid'] = 'Please select a valid country.';
$_['gender_invalid'] = 'Sorry we didn\'t know about that one.';
$_['bio_invalid'] = 'Your bio seems to contain invalid characters.';
$_['password_match'] = 'The passwords you entered did not match.';
$_['password_changed'] = 'Your password has been changed.';
// Activate
$_['activate_mail_sent'] = 'An activation email has been sent to your inbox.';
$_['activate_mail_fail'] = 'The activation email could not be sent.';
$_['key_invalid'] = 'The activation link appears to be not legitimate or expired.';
$_['activation_success'] = 'Your account has been activated.';
$_['activation_fail'] = 'The account could not be activated.';
// Password Recovery
$_['recovery_not_sent'] = 'Error: the recovery link was not sent.';
$_['recovery_sent'] = 'A recovery link has been sent to <b><i>{{email}}</i></b>.';
$_['invalid_token_text'] = 'Error: The recovery token is invalid or expired.';
// File Uploads
$_['file_invalid'] = 'File denied. Excepted files types for avatars are: jpg | jpeg | png | gif.';
$_['file_big'] = 'File is too big <b><i>{{filesize}}MB</b></i>. Max filesize: {{maxFilesize}}MB.';
$_['upload_failure'] = 'Your file has been uploaded but with some errors.';
$_['upload_success'] = 'Your avatar has been updated.';

/**
 * Logs
 */
$_['log_avatar_change'] = '<b><i>{{name}}</i></b> changed their account picture.';
$_['log_account_update'] = '<b><i>{{name}}</i></b> updated their account. <b><i>{{key}}: {{before}} / {{after}}</i></b>.';
$_['log_reset_requested'] = 'A user sent a password reset request to <b><i>{{email}}</i></b>';
$_['log_password_reset'] = '<b><i>{{name}}</i></b> has reset their password.';
$_['log_password_reset'] = '<b><i>{{name}}</i></b> changed their password.';