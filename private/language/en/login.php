<?php

/**
 * Title
 */
$_['title'] = 'Login';

/**
 * Description
 */
$_['description'] = 'This is the login page description and it is about 160 characters long, which is super important for seo or \'search engine optimization\'. Try to keep it so.';

/**
 * Alerts
 */
$_['login_success'] = 'User <b><i>{{name}}</i></b> logged in.';
$_['login_fail'] = 'The email and/or password you entered was incorrect.';
$_['locked'] = 'This account has been locked becuase of too many failed login attempts.';
$_['activation_pending'] = 'Your account is not yet activated.';

/**
 * Logs
 */
$_['log_unregistered'] = 'A User attempted to log in with an unregistered email \'{{email}}\'.';
$_['log_locked'] = 'A banned/locked user attempted to log in. \'{{email}}\'.';
$_['log_unactivated'] = 'An un-activated user attempted to log in. \'{{email}}\'.';
$_['log_attempt'] = 'User attempted to log in using the email address \'{{email}}\'. Attempt #{{attempts}}.';