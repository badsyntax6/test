<?php
/**
 * HTTP status codes
 */

// 100
$_['unknown'] = 'Unknown status code';

// 100
$_['100'] = '100 - Continue';
$_['101'] = '101 - Switching Protocols';
$_['102'] = '102 - Processing';
$_['103'] = '103 - Early Hints';

// 200
$_['200'] = '200 - OK';
$_['201'] = '201 - Created';
$_['202'] = '202 - Accepted';
$_['203'] = '203 - Non-Authoritative Information';
$_['204'] = '204 - No Content';
$_['205'] = '205 - Reset Content';
$_['206'] = '206 - Partial Content';
$_['207'] = '207 - Multi-Status';
$_['208'] = '208 - Already Reported';
$_['226'] = '226 - IM Used';

// 300
$_['300'] = '300 - Multiple Choices';
$_['301'] = '301 - Moved Permanently';
$_['302'] = '302 - Found';
$_['303'] = '303 - See Other';
$_['304'] = '304 - Not Modified';
$_['305'] = '305 - Use Proxy';
$_['306'] = '306 - Switch Proxy';
$_['307'] = '307 - Temporary Redirect';
$_['308'] = '308 - Permanent Redirect';

// 400
$_['400'] = '400 - Bad Request';
$_['401'] = '401 - Unauthorized';
$_['402'] = '402 - Payment Required';
$_['403'] = '403 - Forbidden';
$_['404'] = '404 - Not Found';
$_['405'] = '405 - Method Not Allowed';
$_['406'] = '406 - Not Acceptable';
$_['407'] = '407 - Proxy Authentication Required';
$_['408'] = '408 - Request Time-out';
$_['409'] = '409 - Conflict';
$_['410'] = '410 - Gone';
$_['411'] = '411 - Length Required';
$_['412'] = '412 - Precondition Failed';
$_['413'] = '413 - Request Entity Too Large';
$_['414'] = '414 - Request-URI Too Large';
$_['415'] = '415 - Unsupported Media Type';

// 500
$_['500'] = '500 - Internal Server Error';
$_['501'] = '501 - Not Implemented';
$_['502'] = '502 - Bad Gateway';
$_['503'] = '503 - Service Unavailable';
$_['504'] = '504 - Gateway Time-out';
$_['505'] = '505 - HTTP Version not supported';