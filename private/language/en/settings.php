<?php 

/**
 * Title
 */
$_['title'] = 'Settings';

/**
 * Alerts
 */
$_['setting_updated'] = 'Settings have been updated.';
$_['logs_cleared'] = 'Logs cleared.';
$_['errors_cleared'] = 'Errors cleared.';
$_['email_save_fail'] = 'Could not update the email setting.';
$_['pw_save_fail'] = 'Could not update the password setting.';
$_['inactive_save_fail'] = 'Could not update the inactivity setting.';
$_['lang_save_fail'] = 'Could not update the language setting.';
$_['maint_save_fail'] = 'Could not update the maintenance setting.';
$_['mail_save_fail'] = 'Could not save mail settings.';
$_['mail_update_fail'] = 'Could not update mail settings.';
$_['email_sent'] = 'The test email was sent.';
$_['email_fail'] = 'The test email could not be sent.';