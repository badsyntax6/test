<?php 

/**
 * Title
 */
$_['title'] = 'Signup';

/**
 * Description
 */
$_['description'] = 'This is the signup page description and it is about 160 characters long, which is super important for seo or \'search engine optimization\'. Try to keep it so.';

/**
 * Alerts
 */
$_['username_taken'] = 'The username you have chosen is already taken.';
$_['username_invalid'] = 'Usernames must be letters A-Z and no more than 20 characters long.';
$_['firstname_invalid'] = 'First name should be letters a-z and no spaces.';
$_['lastname_invalid'] = 'Last name should be letters a-z and no spaces.';
$_['email_taken'] = 'The email address you have chosen is already taken.';
$_['email_invalid'] = 'The email address you entered was invalid.';
$_['password_weak'] = 'The password is too weak. <br><b>Passwords must:</b> <br>- be 8 or more characters <br>- Have at least 1 number. <br>- Have at least 1 upper case letter. <br>- Contain one of the following \'!@#$%\'.';
$_['password_match'] = 'The passwords you entered do not match.';
$_['signup_success'] = 'Your account has been created and is awaiting activation. You will receive a notification when youre account is activated.';
$_['signup_fail'] = 'Unable to create your account. Please contact an administrator if the proplem persists.';
$_['user_banned'] = 'This account has been permanently banned.';
$_['activate_mail_fail'] = 'Your account was created but we were unable to send an activation email. Please contact an administrator if the problem persists.';

/**
 * Logs
 */
$_['log_signup_attempt'] = 'Error: A sign up attempt was made using the email \'{{email}}\'. Warning: {{errors}}';
$_['log_new_user'] = 'A new user \'{{name}}\' registered an account.';