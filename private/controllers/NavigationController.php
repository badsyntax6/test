<?php 

/**
 * Navigation Controller Class
 *
 * The NavigationController handles logic specific to the header and displays the 
 * header view. The NavigationController should be loaded in each controller class 
 * where a header is desired.
 */
class NavigationController extends Controller
{
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Assuming that the NavigationController should be loaded in every controller 
     * this index method should run in every controller too.
     */
    public function index()
    {    
        $view['nav_text_dashboard'] = Language::get('nav/dashboard');
        $view['nav_text_users'] = Language::get('nav/users');
        $view['nav_text_calendar'] = Language::get('nav/calendar');
        $view['nav_text_filebrowser'] = Language::get('nav/filebrowser');
        $view['nav_text_settings'] = Language::get('nav/settings');
        $view['nav_text_logout'] = Language::get('nav/logout');

        if (Auth::isLogged()) {
            $view['user_level'] = Auth::group();
            $view['menu'] = Auth::user()->main_menu == 1 ? 'wide' : '';
            $view['menu_toggle'] = Auth::user()->main_menu == 1 ? 'on' : 'off';
        }

        return Load::view('common/nav', $view);  
    }
}