<?php 

/**
 * Dashboard Controller Class
 */
class DashboardController extends Controller
{
    /**
     * Init method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/dashboard
     * - http://root/dashboard/index
     *
     * The DashboardController class is the default controller for the application. 
     * This means that invalid routes will default to this index method.
     */
    public function index()
    {   
        $users = $this->getUserData();
        $settings = $this->getSettingsData();
        $logs = Load::model('log')->getLogs();
        $errors = Load::model('log')->getErrors();

        foreach ($users as $key => $value) {
            $view[$key] = $value;
        }

        foreach ($settings as $key => $value) {
            $view[$key] = $value;
        }

        $view['logs'] = $this->formatLogs($logs);
        $view['errors'] = $this->formatLogs($errors);
        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();
        $view['nav'] = Load::controller('navigation')->index();
        $view['breadcrumb'] = Load::controller('breadcrumb')->index();
        $view['user_group'] = Auth::group();

        Output::html('common/dashboard', $view);
    }

    /** 
     * Format logs for readability
     *
     * @return array
     */
    private function formatLogs($logs = [])
    {
        if (!empty($logs)) {
            foreach ($logs as &$log) {
                $log['time'] = date('d/m/Y h:ia', strtotime($log['time']));
            }
        }
        return $logs;
    }

    /**
     * Get user data
     *
     * @return array
     */
    private function getUserData()
    {
        $model = Load::model('user');
        $total_users = $model->getUsersNumber();
        $registered = $model->getUsersNumber(1);
        $admins = $model->getUsersNumber(3);

        $data['total_users'] = $total_users ? $total_users : 0;
        $data['registered'] = $registered ? $registered : 0;
        $data['admins'] = $admins ? $admins : 0;

        return $data;
    }

    /**
     * Get settings data
     *
     * @return array
     */
    private function getSettingsData()
    {
        $model = Load::model('settings');
        $settings = $model->getSettings('language');

        foreach ($settings as $s) {
            switch ($s['name']) {
                case 'owners_email': $data[$s['name']] = $s['value']; break;
                case 'strong_pw': $data[$s['name']] = isset($s['value']) ? $s['value'] : 'Off' ; break;
                case 'inactivity_limit': $data[$s['name']] = $s['value']; break;
                case 'language': $data[$s['name']] = ucfirst($s['value']); break;
                case 'owners_email': $data[$s['name']] = $s['value']; break;
                case 'maintenance_mode': $data[$s['name']] = isset($s['value']) ? $s['value'] : 'Off' ; break;
                default: $data[$s['name']] = $s['value']; break;
            }
        }

        return $data;
    }

    /**
     * Clear log
     * 
     * This method truncates the log table in the database
     *
     * @return void
     */
    public function clearLog()
    {
        if (Load::model('log')->clearLog()) {
            $output['alert'] = 'success';
            $output['message'] = Language::get('settings/logs_cleared');
            Output::json($output);
        }
    }

    /**
     * Clear errors
     * 
     * This method truncates the error table in the database
     *
     * @return void
     */
    public function clearErrors()
    {
        if (Load::model('log')->clearErrors()) {
            $output['alert'] = 'success';
            $output['message'] = Language::get('settings/errors_cleared');
            Output::json($output);
        }
    }
}