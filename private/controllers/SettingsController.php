<?php 

/**
 * Settings Controller Class
 */
class SettingsController extends Controller
{
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/settings
     * - http://root/settings/index
     *
     * This method will load settings view.
     */
    public function index($settings = 'settings')
    {
        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();
        $view['nav'] = Load::controller('navigation')->index();
        $view['breadcrumb'] = Load::controller('breadcrumb')->index();
        $view['buttons'] = Load::view('common/buttons');
        $view['languages'] = Load::model('settings')->getLanguages();

        Output::html('settings/' . $settings, $view);
    }

    public function update($update)
    {
        $this->settings_model = Load::model('settings');

        if (isset($update) && $update == 'general') {
            $this->updateOwnersEmailSetting();
            $this->updatePasswordSetting();
            $this->updateInactiveSetting();
            $this->updateLanguageSetting();
        }

        if (isset($update) && $update == 'mail') {
            $this->updateMailSettings();
        }

        if (isset($update) && $update == 'server') {
            $this->updateMaintenanceSetting();
        }
        
        Log::event('"' . Auth::firstname() . ' ' . Auth::lastname() . '" updated the site settings.');
        $output = ['alert' => 'success', 'message' => Language::get('settings/setting_updated')];
        Output::json($output);
    }

    public function updateOwnersEmailSetting()
    {
        $data['value'] = $_POST['owners_email'];
        $data['setting_id'] = 1;

        if (!$this->settings_model->updateSetting($data)) {
            $output = ['alert' => 'error', 'message' => Language::get('settings/email_save_fail')];
            exit(Output::json($output));
        }
    }

    public function updatePasswordSetting()
    {
        $data['value'] = isset($_POST['strong_pw']) ? 1 : null;
        $data['setting_id'] = 2;

        if (!$this->settings_model->updateSetting($data)) {
            $output = ['alert' => 'error', 'message' => Language::get('settings/pw_save_fail')];
            exit(Output::json($output));
        }
    }

    public function updateInactiveSetting()
    {
        $data['value'] = $_POST['inactivity_limit'];
        $data['setting_id'] = 3;

        if (!$this->settings_model->updateSetting($data)) {
            $output = ['alert' => 'error', 'message' => Language::get('settings/inactive_save_fail')];
            exit(Output::json($output));
        }
    }

    public function updateLanguageSetting()
    {
        $data['value'] = $_POST['language'];
        $data['setting_id'] = 4;

        if (!$this->settings_model->updateSetting($data)) {
            $output = ['alert' => 'error', 'message' => Language::get('settings/lang_save_fail')];
            exit(Output::json($output));
        }
    }

    public function updateMailSettings()
    {
        $mail_settings = $this->settings_model->getMailSettings();

        $data['host'] = $_POST['host'];
        $data['port'] = $_POST['port'];
        $data['username'] = $_POST['username'];
        $data['password'] = $_POST['password'];

        if (!$mail_settings) {
            if (!$this->settings_model->insertMailSettings($data)) {
                $output = ['alert' => 'error', 'message' => Language::get('settings/mail_save_fail')];
                exit(Output::json($output));
            }
        }

        if ($mail_settings) {

            $data['mail_id'] = 1;

            if (!$this->settings_model->updateMailSettings($data)) {
                $output = ['alert' => 'error', 'message' => Language::get('settings/mail_update_fail')];
                exit(Output::json($output));
            }
        }
    }

    public function updateMaintenanceSetting()
    {
        $data['value'] = !empty($_POST['maintenance_mode']) ? 1 : null;
        $data['setting_id'] = 5;

        if (!$this->settings_model->updateSetting($data)) {
            $output = ['alert' => 'error', 'message' => Language::get('settings/maint_update_fail')];
            exit(Output::json($output));
        }
    }

    public function changeMenuSetting() 
    {       
        $data['main_menu'] = $_POST['menu_status'];
        $data['menu_anchor'] = Auth::id();

        Load::model('user')->updateUserMenuSetting($data);

        Output::json($data['main_menu']);
    }

    public function getSettingsJson()
    {
        $model = Load::model('settings');
        $settings = $model->getSettings();
        $mail = $model->getMailSettings();

        foreach ($settings as $s) {
            if (isset($s['value'])) {
                $data[$s['name']] = $s['value'];
            }
        }

        if ($mail) {
            foreach ($mail as $key => $value) {
                $data[$key] = $value;
            }
        }

        Output::json($data);
    }

    public function testEmail()
    {
        $sitename = Load::model('settings')->getSetting('sitename');
        $owners_email = Load::model('settings')->getSetting('owners_email');
        $mail_library = Load::library('mail');

        $mail['to'] = $owners_email;
        $mail['from'] = '';
        $mail['subject'] = $sitename . ' Test Email';
        $mail['message'] = 'Success! This is a test email from ' . $sitename;
        $mail['body'] = $mail['message'];

        if ($mail_library->sendMail($mail)) {
            $output = ['alert' => 'success', 'message' => Language::get('settings/email_sent')];
        } else {
            $output = ['alert' => 'error', 'message' => Language::get('settings/email_fail')];
        }

        Output::json($output);
    }

    /**
     * Get the inactiviy limit.
     *
     * This method is called by ajax /public/javascript/activity.js.
     *
     * Expected output should be an int between 0 and 60.
     * The int represents minutes.
     */
    public function getInactivityLimit()
    {
        $output = Load::model('settings')->getSetting('inactivity_limit');
        Output::json((int) $output);
    }
}