<?php 

/**
 * Filebrowser Controller Class
 * 
 * This controller is tightly coupled with the filebrowser library. It handles input 
 * from the user and displays output to the user. It is also tightly coupled with
 * the filebrowser javascript file.
 * 
 * @see Filebrowser Library Class - /root/private/libraries/Filebrowser.php
 * @see Filebrowser JS - /root/public/javascript/filebrowser.php
 */
class FilebrowserController extends Controller
{
    public function index()
    {
        $view['header'] = Load::controller('header')->index('Filebrowser');
        $view['footer'] = Load::controller('footer')->index();
        $view['nav'] = Load::controller('navigation')->index();
        $view['breadcrumb'] = Load::controller('breadcrumb')->index();

        Output::html('common/filebrowser', $view);
    }

    /**
     * Browse a directory
     *
     * @return void
     */
    public function browse()
    {
        $result = Load::library('filebrowser')->browse();
        Output::json($result);
    }

    /**
     * Make a new folder
     *
     * @return void
     */
    public function makeFolder()
    {
        $foldername = $_POST['foldername'];
        $directory = PUBLIC_DIR . $_POST['new_folder_dir'];

        if (Load::library('filebrowser')->makeFolder($foldername, $directory)) exit(Output::json(['alert' => 'success', 'message' => Language::get('filebrowser/folder_created')]));
        else exit(Output::json(['alert' => 'success', 'message' => Language::get('filebrowser/folder_exists')]));
    }

    public function delete() 
    {
        $directory = PUBLIC_DIR . $_POST['dir'];
        $files = $_POST['filenames'];
        foreach ($files as $f) {
            $filebrowser = Load::library('filebrowser')->delete($directory, $f['value']);
            if ($filebrowser->system_file) return Output::json(['alert' => 'error', 'message' => Language::get('filebrowser/system_file')]);
        }
        if ($filebrowser->file_deleted) Output::json(['alert' => 'success', 'message' => Language::get('filebrowser/file_deleted')]);
    }

    public function upload()
    {
        $upload_dir = $_POST['upload_dir'];
        $upload = Load::library('Upload');
        $image = Load:: library('Image');
        $is_image = getimagesize($_FILES['upload_item']['tmp_name']) ? true : false;

        $upload->uploadAnything($_FILES['upload_item'], $upload_dir);

        if ($upload->file_exists) {
            exit(Output::json(['alert' => 'error', 'message' => Language::get('filebrowser/file_exists', ['name' => $_FILES['upload_item']['name']])]));
        }
        if ($upload->file_invalid) {
            if ($is_image) exit(Output::json(['alert' => 'error', 'message' => Language::get('filebrowser/file_invalid', ['types' => 'jpg | jpeg | png | gif | tif | tiff | svg'])]));
            if (!$is_image) exit(Output::json(['alert' => 'error', 'message' => Language::get('filebrowser/file_invalid', ['types' => 'txt | pdf | doc | docx | xls | xml'])]));
        }
        if ($upload->file_big) exit(Output::json(['alert' => 'error', 'message' => Language::get('filebrowser/file_big', ['size' => $upload->filesize, 'maxsize' => $upload->max_size])]));
        if ($source_image = $upload->upload_success) {
            if ($is_image) $image->makeThumbnail($source_image['source'], $source_image['dir'], 100);
            Log::event(Auth::fullname() . ' uploaded images "' . $_FILES['upload_item']['name'] . '".');

            exit(Output::json(['alert' => 'success', 'message' => Language::get('filebrowser/file_uploaded')]));
        }
    }
}