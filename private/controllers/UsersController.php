<?php 

/**
 * Users Controller Class
 *
 * This class gets users information and has the ability to alter a users status.
 */
class UsersController extends Controller
{
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/users
     * - http://root/users/index
     *
     * This method will load the users list table.
     */
    public function index()
    {
        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();
        $view['nav'] = Load::controller('navigation')->index();
        $view['breadcrumb'] = Load::controller('breadcrumb')->index();
        $view['pagination'] = Load::view('common/pagination', ['filters' => Load::view('users/filters')]);
        $view['buttons'] = Load::view('common/buttons');
        $view['controls'] = Load::view('users/controls', ['group' => Auth::group()]);
        $view['url'] = '/users/getTable';

        Output::html('common/list', $view);
    }

    /**
     * Get paginated records for a table
     * 
     * Get user records, paginate them, and output them as json. This method is tightly
     * coupled with the pagination javascript file.
     * 
     * @see pagination JS - /root/public/javascript/pagination.js
     * @see Pagination Model - /root/private/models/PaginationModel.php
     *
     * @param string $table - The table to get data from
     * @param string $orderby - What to order the data by
     * @param string $direction - Oldest or newest first
     * @param integer $page - What page to start on. Usually page 1
     * @param integer $record_limit - How many records to display per page
     * @param string $column - If we sort which column to sort
     * @param string $is - If we sort wich data in the column do we want to see
     * @return void
     */
    public function getTable($table = 'user', $orderby = 'user_id', $direction = 'asc', $page = 1, $record_limit = 15, $column = null, $is = null)
    {
        $orderby = empty($_POST['orderby']) ? $orderby : $_POST['orderby'];
        $direction = empty($_POST['direction']) ? $direction : $_POST['direction'];
        $page = empty($_POST['page']) ? $page : $_POST['page'];
        $record_limit = empty($_POST['record_limit']) ? $record_limit : $_POST['record_limit'];
        $column = empty($_POST['column']) ? $column : $_POST['column'];
        $is = empty($_POST['is']) ? $is : $_POST['is'];
        
        $paginated = Load::model('pagination')->paginate($table, $orderby, $direction, $page, $record_limit, $column, $is);

        $view['users'] = [];

        foreach ($paginated['records'] as $user) {
            switch ($user['group']) {
                case '2': $group = 'Registered'; break;
                case '3': $group = 'Moderator'; break;
                case '4': $group = 'Admin'; break;
                case '0': $group = 'Locked'; break;
                default: $group = 'Activation pending'; break;
            }
            
            $view['users'][] = [
                'user_id' => $user['user_id'],
                'firstname' => $user['firstname'],
                'lastname' => $user['lastname'],
                'username' => $user['username'],
                'email' => $user['email'],
                'signup_date' => date('d M, Y', strtotime($user['signup_date'])),
                'status' => $this->userOnline($user['last_active']) ? 'Online' : 'Offline',
                'group' => $group,
                'group_num' => $user['group']
            ];
        }

        $output = [
            'list' => Load::view('users/list', $view),
            'table' => $table,
            'orderby' => $orderby,
            'direction' => $direction,
            'record_limit' => $record_limit,
            'page' => $page,
            'start' => $paginated['start'],
            'total_pages' => $paginated['pages'],
            'total_records' => $paginated['total']
        ];

        Output::json($output);
    }

    /**
     * Get and display user data
     *
     * @param string $name - users name
     * @return void
     */
    public function user($name = null)
    {       
        if (is_null($name)) Load::route('/users');

        $name = explode('-', $name);
        $firstname = $name[0];
        $lastname = $name[1];
        $user = Load::model('user')->getUserProfile($firstname, $lastname);
        
        if (!$user) Load::route('/users');

        $is_online = $this->userOnline($user['last_active']);
        $la_days_ago = getDaysAgo($user['last_active']);
        $sd_days_ago = getDaysAgo($user['signup_date']);

        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();
        $view['nav'] = Load::controller('navigation')->index();
        $view['breadcrumb'] = Load::controller('breadcrumb')->index();
        $view['buttons'] = Load::view('common/buttons');

        if ($user['user_id'] == Auth::id()) {
            $view['self'] = true;
        } else {
            $view['self'] = null;
        }

        $view['user_id'] = $user['user_id'];
        switch ($user['group']) {
            case 0: $view['group'] = 'Locked'; break;
            case 1: $view['group'] = 'Activation pending'; break;
            case 2: $view['group'] = 'Registered'; break;
            case 3: $view['group'] = 'Moderator'; break;
            case 4: $view['group'] = 'Administrator'; break;
        }
        $view['firstname'] = $user['firstname'];
        $view['lastname'] = $user['lastname'];
        $view['username'] = $user['username'];
        $view['email'] = $user['email'];
        $view['phone'] = $user['phone'];
        $view['country'] = $user['country'];
        $view['birthday'] = date('d M, Y', strtotime($user['birthday']));
        $view['gender'] = $user['gender'];
        $view['bio'] = $user['bio'];
        $view['registered'] = date('d M, Y', strtotime($user['signup_date']));
        $view['last_active'] = isset($user['last_active']) ? date('d M, Y', strtotime($user['last_active'])) : 'Never';
        if ($user['privacy'] == 0) $view['privacy'] = 'Public'; 
        if ($user['privacy'] == 1) $view['privacy'] = 'Private';
        if ($user['privacy'] == 2) $view['privacy'] = 'Locked';
        $view['avatar'] = $user['avatar'];
        $view['status'] = $is_online ? 'Online' : 'Offline';
        if ($la_days_ago !== false) {
            $view['la_days_ago'] = $la_days_ago == '0' ? '(Today)' : '(' . $la_days_ago . ' days ago)';
        } else {
            $view['la_days_ago'] = '';
        }
        $view['sd_days_ago'] = '(' . $sd_days_ago . ' days ago)';

        Output::html('users/user', $view);
    }

    /**
     * Check if the user is online
     * 
     * If the users last activity was more than 5 minutes ago we assume they are no longer online.
     *
     * @param string $last_active - Date the user was last active
     * @return bool
     */
    private function userOnline($last_active) 
    {
        $last_active = strtotime($last_active);

        if (time() - $last_active > 5 * 60) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Edit a user
     * 
     * This method really only changes the users group level. So its mostly
     * used to activate and ban users... mostly.
     *
     * @return void
     */
    public function edit()
    {
        $model = Load::model('user');
        $group = $_POST['group'];

        foreach ($_POST['ids'] as $id) {
            $user = $model->getUser('user_id', $id['value']);

            if ($user) {
                switch ($group) {
                    case '0': $group_text = 'Locked'; break;
                    case '1': $group_text = 'Un-registered'; break;
                    case '2': $group_text = 'Registered'; break;
                    case '3': $group_text = 'Moderator'; break;
                    case '4': $group_text = 'Administrator'; break;
                    default: $group_text = 'Unknown'; break;
                }

                $data['group'] = $group;
                $data['user_id'] = $id['value'];

                $output['group'] = $group;

                $log = ['admin' => Auth::firstname() . ' ' . Auth::lastname(), 'name' => $user['firstname'] . ' ' . $user['lastname'], 'group' => $group_text];

                if ($model->updateUser($data, 'user_id')) {
                    $output = ['alert' => 'success', 'message' => Language::get('users/user_updated')];
                    Log::event(Language::get('users/log_user_update', $log));
                } else {
                    $output = ['alert' => 'error', 'message' => Language::get('users/user_not_updated')];
                    Log::event(Language::get('users/log_user_update_fail', $log));
                }
            }
        }

        Output::json($output);
    }

    /**
     * Delete a user
     * 
     * This method will completely remove a user from the database.
     * This is not a soft delete, they will be gone.
     *
     * @return void
     */
    public function delete()
    {
        $model = Load::model('user');

        foreach ($_POST as $id) {
            $user = $model->getUser('user_id', $id);
            if ($user && $user['user_id'] != 1) {
                if ($model->deleteUser($id)) {
                    $output = ['alert' => 'success', 'message' => Language::get('users/users_deleted')];
                    Log::event('Admin "' . Auth::firstname() . ' ' . Auth::lastname() .  '" deleted user "' . $user['firstname'] . ' ' . $user['lastname'] . '".');
                } else {
                    $output = ['alert' => 'error', 'message' => 'User delete failed.'];
                    Log::event('Admin "' . Auth::firstname() . ' ' . Auth::lastname() .  '" was unable to delete user "' . $user['firstname'] . ' ' . $user['lastname'] . '". Check error logs.');
                }
            }   
        }

        Output::json($output);
    }
}