<?php 

/**
 * Test Controller
 * 
 * Have fun
 */
class TestController extends Controller
{
    /* public function beforeFilter()
    {
        echo 'This is before <br>';
    }

    public function afterFilter()
    {
        
    } */

    public function index()
    {     
        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();
        $view['nav'] = Load::controller('navigation')->index();
        $view['breadcrumb'] = Load::controller('breadcrumb')->index();
        $view['year'] = date('Y');
        $view['countries'] = Load::model('user')->getCountries();

        Output::html('common/test', $view);
    }

    public function json() 
    {
        $arr = ['foo' => 'f', 'bar' => 'b', 'fing' => 'f'];

        Output::json($arr);
    }

    public function dateToArray($date)
    {
        $date = explode('-', $date);

        $array['day'] = $date[0];
        $array['date'] = $date[1];
        $array['time'] = end($date);

        return $array;
    }
    
    public function ex()
    {
        echo Language::get('test/foo', ['food' => 'pizza', 'name' => 'chase', 'age' => '32']);
    }

    /**
     * Check Day
     *
     * Compare date against todays date and see if the date is today, tomorrow, yesterday or some other time.
     */
    public function checkDay()
    {
        $timestamp = new DateTime('2018-12-27');
        $today = new DateTime(); // This object represents current date/time

        $timestamp->setTime(0, 0, 0); // reset time part, to prevent partial comparison
        $today->setTime(0, 0, 0); // reset time part, to prevent partial comparison

        $diff = $today->diff($timestamp);
        $diff_days = (int)$diff->format('%R%a'); // Extract days count in interval

        switch($diff_days) {
            case 0:
                echo '//Today';
                break;
            case -1:
                echo '//Yesterday';
                break;
            case +1:
                echo '//Tomorrow';
                break;
            default:
                echo '//Sometime';
        }
    }

    public function tryCountdown()
    {
        $date = strtotime('December 25, 2018 12:00 am');
        $remaining = $date - time();
        $days_remaining = floor($remaining / 86400);
        $hours_remaining = floor(($remaining % 86400) / 3600);
        if ($days_remaining > 0) {
            echo 'There are ' . $days_remaining . ' days and ' . $hours_remaining . ' hours left';
        } else {
            echo 'The time is past already';
        }
    }

    public function validate()
    {
        $email = 'chase@gusto.com';
        $tel = '12345678901234567';
        $num = '45';
        $url = 'http://foobar';
        $words = 'Hello World';
        $alpha = 'Foo';
        $alpha_num = '123foo456';
        $text = 'Whatever is @!? foo bar baz %$';
        $custom = 'abc'; 
        
        if (Validate::num($num)) {
            echo 'SUCCESS! ' . $num . ' is valid.';
        } else {
            echo 'ERROR! ' . $num . ' is invalid.';
        }

        echo '<pre>';
        var_dump(Validate::min(8)->max(20)->required()->email($email)); // Email valid and minimum 8 chars and maximum 20 chars
        var_dump(Validate::tel($tel));
        var_dump(Validate::num($num));
        var_dump(Validate::url($url));
        var_dump(Validate::words($words));
        var_dump(Validate::alpha($alpha));
        var_dump(Validate::alphaNum($alpha_num));
        var_dump(Validate::text($text));
        var_dump(Validate::custom($custom, '^[a-zA-Z]{2,4}$')); // Custom regex patter ^[a-zA-Z0-9]{1,4}$ will match string with alphabet and numbers between 2 and 4 chars long
        echo '</pre>';
    }

    public function getDate()
    {
        //7.8.19-03.08.28
        exit(date('d.m.y-g.i.s'));
    }

    public function test()
    {
        echo 'This is test';
    }

    public function testLive()
    {
        echo 'This is test LIVE';
    }
}