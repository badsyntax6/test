<?php

/**
 * Account Controller Class
 *
 * This class gets the users data from the database for display on the 
 * account view. It also handles edits to the account that are made by the 
 * account holder, activation and password resets.
 */
class AccountController extends Controller
{   
    /**
     * Array of account data to be updated
     * @var array
     */
    public $update = [];

    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/account
     * - http://root/account/index
     *
     * This index method gets user data from the database using the user session.
     * This method will also prepare the data for the account view.
     */
    public function index()
    {
        $view['fullname'] = Auth::fullname();
        $view['avatar'] = Auth::user()->avatar;
        $view['year'] = date('Y');
        $view['theme_text'] = Language::get('header/theme_text');
        $view['countries'] = Load::model('user')->getCountries();

        $crumbs = [
            ['link' => 'account', 'crumb' => 'ACCOUNT'], 
            ['link' => 'account', 'crumb' => strtoupper(Auth::fullname())]
        ];

        $view['breadcrumb'] = Load::controller('breadcrumb')->index($crumbs);
        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();
        $view['nav'] = Load::controller('navigation')->index();
        $view['buttons'] = Load::view('common/buttons');

        $is_online = $this->userOnline(Auth::user()->last_active);

        $view['status'] = $is_online ? 'Online' : 'Offline';

        Output::html('account/account', $view);
    }

    /**
     * Check if the user is online
     * 
     * Get the time the user was last active and verify
     * it was less than or greater 5 minutes ago.
     *
     * @param string $last_active - Time the user last moved their mouse or pressed a key
     * @return bool
     */
    private function userOnline($last_active) 
    {
        $last_active = strtotime($last_active);

        if (time() - $last_active > 5 * 60) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get account info in json format
     * 
     * Output account info in json format so the account for can be more
     * easily populated with deserialize.js.
     *
     * @see root/public/javascript/deserialize.js
     * @return void
     */
    public function getAccountJson()
    {
        $user = Load::model('user')->getUser('user_id', Auth::id());

        $data['firstname'] = $user['firstname'];
        $data['lastname'] = $user['lastname'];
        $data['email'] = $user['email'];
        $data['phone'] = $user['phone'];
        $data['theme'] = $user['theme'];
        $data['day'] = date('j', strtotime($user['birthday']));
        $data['month'] = date('m', strtotime($user['birthday']));
        $data['year'] = date('Y', strtotime($user['birthday']));
        $data['country'] = $user['country'];
        $data['gender'] = $user['gender'];
        $data['bio'] = $user['bio'];
        $data['foo'] = 'foo';

        if ($user['verify_logout']) {
            $data['logout_setting'] = 1;
        }

        Output::json($data);
    }

    /**
     * Upload Avatar Image
     *
     * This method will be called by the dropzone ajax on the account page.
     * This method is responsible for validating the file and uploading it
     * to the server. It makes use of the Upload and Image libraries.
     * 
     * @see Upload Library Class - /root/private/libraries/Upload.php 
     * @see Upload Library Class - /root/private/libraries/Image.php 
     * @return bool
     */
    public function uploadAvatar()
    {
        if (!empty($_FILES['avatar'])) {

            $upload_lib = Load::library('Upload');
            $image_lib = Load::library('image');
            $dir = '/uploads/account/';

            $upload_lib->uploadImage($_FILES['avatar'], $dir);

            if ($upload_lib->file_invalid) exit(Language::get('account/file_invalid'));
            if ($upload_lib->file_big) exit(Language::get('account/file_big'));
            if ($upload_lib->upload_success) {
                $data['avatar'] = $upload_lib->filename;
                $data['user_id'] = Auth::id();

                $source = PUBLIC_DIR . '/uploads/account/' . $upload_lib->filename;
                $crop_image = $image_lib->cropImage($source, 256, 256, $upload_lib->filename, $dir);
                $update = Load::model('user')->updateUser($data, 'user_id');

                if ($update) {
                    if ($crop_image) {
                        Log::event(str_replace('{{name}}',  Auth::fullname(), Language::get('account/log_avatar_change')));
                        $output = ['alert' => 'success', 'message' => Language::get('account/upload_success'), 'filename' => $upload_lib->filename];
                    } else {
                        $output = ['alert' => 'error', 'message' => Language::get('account/upload_failure')];
                    }
                }

                return Output::json($output);
            }
        }
    }

    /**
     * Validate account changes
     *
     * This method validates all changes to the users account. When users hit save
     * on the account page the form is submitted via ajax to this method.
     */
    public function validate()
    {
        $this->validateUsername();
        $this->validateFirstname();
        $this->validateLastname();
        $this->validatePhone();
        $this->validateEmail();
        $this->validateLogout();
        $this->validateBirthday();
        $this->validateCountry();
        $this->validateGender();
        $this->validateBio();
        $this->validatePassword();

        $model = Load::model('user');
        $user_before = $model->getUser('user_id', Auth::id());

        $this->update['user_id'] = Auth::id();

        if ($model->updateUser($this->update, 'user_id')) {

            $user_after = $model->getUser('user_id', Auth::id());
            $changed = array_diff($user_after, $user_before);

            foreach ($changed as $key => $value) {

                if ($key == 'birthday') {
                    $value = date('d M Y', strtotime($value));
                    $user_before[$key] = date('d M Y', strtotime($user_before[$key]));
                }

                Log::event(Language::get('account/log_account_update', ['name' => Auth::fullname(), 'key' => ucfirst($key), 'before' => $user_before[$key], 'after' => $value]));
            }
        }

        $output = ['alert' => 'success', 'message' => Language::get('account/account_updated')];
        return Output::json($output);
    }

    /**
     * Validate username
     * 
     * Sanitizes and validates the username input record.
     */
    private function validateUsername()
    {
        if (!empty($_POST['username'])) {
            $username = trim(str_replace(' ', '', preg_replace('/[^A-Za-z0-9]/', '', $_POST['username'])));

            if ($username != Auth::username()) {

                if (strlen($username) > 20) {
                    exit(Language::get('account/username_invalid'));
                }

                // Check if the username entered is already taken.
                // Here we check if $user is falsy. If it is, we assume no user with the entered username exists.
                // If $user is not falsy then it returned a users data. So we compare the found users id with the logged users id.
                // If the ids do not match then the username is taken. If they do we found the logged user and skip ahead.
                if ($user = $this->model->getUser('username', $username)) {
                    if ($user['user_id'] != Auth::id()) {
                        $output = ['alert' => 'error', 'message' => Language::get('account/username_taken')];
                        return Output::json($output);
                    }
                }

                $this->update['username'] = $username;

                if (!$this->model->updateUser($this->update, 'user_id')) {
                    $output = ['alert' => 'error', 'message' => Language::get('account/username_fail')];
                    return Output::json($output);
                }

                Log::event('"' . Auth::fullname() . '" changed their user name.');
            }
        }
    }

    /**
     * Validate firstname
     *
     * Sanitizes and validates the firstname input field.
     */
    private function validateFirstname()
    {
        if (Validate::required()->alpha($_POST['firstname'])) {
            $this->update['firstname'] = trim($_POST['firstname']);
        } else {
            exit(Output::json(['alert' => 'error', 'message' => Language::get('signup/firstname_invalid')]));
        }
    }

    /**
     * Validate lastname
     *
     * Sanitizes and validates the lastname input field.
     */
    private function validateLastname()
    {
        if (Validate::required()->alpha($_POST['lastname'])) {
            $this->update['lastname'] = trim($_POST['lastname']);
        } else {
            exit(Output::json(['alert' => 'error', 'message' => Language::get('account/lastname_invalid')]));
        }
    }

    /**
     * Validate phone
     *
     * Sanitizes and validates the phone input field.
     */
    private function validatePhone()
    {
        if (Validate::tel($_POST['phone'])) {
            $this->update['phone'] = trim($_POST['phone']);
        } else {
            exit(Output::json(['alert' => 'error', 'message' => Language::get('account/phone_invalid')]));
        }
    }

    /**
     * Validate email
     *
     * Sanitizes and validates the email input field.
     */
    private function validateEmail()
    {
        if (Validate::required()->email($_POST['email'])) {
            $this->update['email'] = trim($_POST['email']);
        } else {
            exit(Output::json(['alert' => 'error', 'message' => Language::get('account/email_invalid')]));
        }

        if ($this->update['email'] != Auth::email()) {
            // Check if the email entered is already taken.
            if (Load::model('user')->getUser('email', $this->update['email'])) {
                exit(Output::json(['alert' => 'error', 'message' => Language::get('account/email_taken')]));
            }
        }
    }

    /**
     * Validate the logout setting
     * 
     * Gusto comes with a verify logout setting. Users may enable or disable
     * this setting. When enabled users will be asked if they are sure they
     * want to log out and need to press a yes button to continue logging out.
     *
     * @return void
     */
    public function validateLogout()
    {
        $verify_logout = isset($_POST['logout_setting']) ? 1 : 0;

        if ($verify_logout != Auth::user()->verify_logout) return;

        $this->update['verify_logout'] = $verify_logout;
    }

    /**
     * Validate users birthday
     *
     * @return void
     */
    public function validateBirthday()
    {
        if (!empty($_POST['day']) && !empty($_POST['month']) && !empty($_POST['year'])) {
            if (!Validate::num($_POST['day'])) {
                exit(Output::json(['alert' => 'error', 'message' => Language::get('account/birthday_invalid')]));
            }
            if (!Validate::num($_POST['month'])) {
                exit(Output::json(['alert' => 'error', 'message' => Language::get('account/birthday_invalid')]));
            }
            if (!Validate::num($_POST['year'])) {
                exit(Output::json(['alert' => 'error', 'message' => Language::get('account/birthday_invalid')]));
            }

            $birthday = date('c', strtotime($_POST['year'] . '-' . $_POST['month'] . '-' . $_POST['day']));

            if (Validate::custom($birthday, '^[A-Z0-9-:]+$')) {
                $this->update['birthday'] = $birthday;
            } else {
                exit(Output::json(['alert' => 'error', 'message' => Language::get('account/birthday_invalid')]));
            }
        }
    }

    /**
     * Validatea users country
     *
     * @return void
     */
    public function validateCountry()
    {
        if (Validate::custom($_POST['country'], '^[a-zA-Z, ]+$')) {
            $this->update['country'] = trim($_POST['country']);
        } else {
            exit(Output::json(['alert' => 'error', 'message' => Language::get('account/country_invalid')]));
        }
    }

    /**
     * Validate users gender
     *
     * @return void
     */
    public function validateGender()
    {
        if (Validate::words($_POST['gender'])) {
            $this->update['gender'] = trim($_POST['gender']);
        } else {
            exit(Output::json(['alert' => 'error', 'message' => Language::get('account/gender_invalid')]));
        }
    }

    /**
     * Validate users bio
     *
     * @return void
     */
    public function validateBio()
    {
        if (Validate::text($_POST['bio'])) {
            $this->update['bio'] = trim($_POST['bio']);
        } else {
            exit(Output::json(['alert' => 'error', 'message' => Language::get('account/bio_invalid')]));
        }
    }

    /**
     * Validate password changes
     *
     * @return void
     */
    private function validatePassword()
    {
        if (!empty($_POST['password']) && !empty($_POST['confirm'])) {
            if (Load::model('settings')->getSetting('strong_pw')) {
                if (!Validate::required()->password($_POST['password'])) {
                    $this->error['password'] = Language::get('signup/password_weak');
                }
            }

            if ($_POST['password'] !== $_POST['confirm']) {
                $this->error['confirm'] = Language::get('signup/password_match');
            } 

            $this->password = password_hash($_POST['password'], PASSWORD_BCRYPT, array('cost' => 12));
            $data['password'] = $this->password;
            $model = Load::model('user');
            $data['user_id'] = Auth::id();

            if ($model->updateUser($data, 'user_id')) {
                Log::event(Language::get('account/log_password_changed', ['name' => Auth::fullname()]));
                Session::delete('id');
                $output = ['alert' => 'success', 'message' => Language::get('account/password_changed'), 'password_changed' => 'true'];
                exit(Output::json($output));
            }
        }
    }

    /**
     * Activate a user account.
     * 
     * @return void
     */
    public function activate()
    {
        if (Auth::group() > 2) Load::route('/dashboard');

        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();

        Output::html('account/activate', $view);
    }

    /**
     * Send account activation email
     *
     * This method sends an email to activate a users account. This method will be called 
     * via an AJAX function which can be found in the account.htm view.
     * 
     * @see root/public/htm/account/account.htm
     * @return void
     */
    public function send()
    {
        $user = Load::model('user')->getUser('email', Auth::email());
        $mail_library = Load::library('mail');
        $link = HOST . '/account/activate/' . $user['key'];

        $mail['to'] = Auth::email();
        $mail['from'] = '';
        $mail['subject'] = 'Activate Your Account';
        $mail['message'] = $link;
        $mail['body'] = str_replace('{{link}}', $link, getTemplate('activate'));

        if ($user) {
            if ($mail_library->sendMail($mail)) {
                $output = ['alert' => 'success', 'message' => Language::get('account/activate_mail_sent')];
            } else {
                $output = ['alert' => 'error', 'message' => Language::get('account/activate_mail_fail')];
            }
        } else {
            $output = ['alert' => 'success', 'message' => Language::get('account/activate_mail_sent')];
        }

        Output::json($output);
    }
    
    /**
     * Display the forgot password page
     *
     * @return void
     */
    public function forgot()
    {
        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();

        Output::html('account/forgot', $view);
    }

    /**
     * Send a password reset request
     *
     * @return void
     */
    public function sendResetRequest()
    {
        $mail_library = Load::library('mail');
        $email = $_POST['email'];
        $token = bin2hex(random_bytes(32));
        $link = HOST . '/account/reset/' . $token;

        Session::create('reset', ['token' => $token, 'email' => $email, 'time' => date('c')], true);

        if (!Validate::required()->email($email)) {
            $output = ['alert' => 'error', 'message' => str_replace('{{email}}', $email, Language::get('account/email_invalid'))];
            exit(Output::json($output));
        }

        $mail['to'] = $email;
        $mail['from'] = '';
        $mail['subject'] = 'Reset Password';
        $mail['message'] = $link;
        $mail['body'] = str_replace('{{link}}', $link, getTemplate('email/reset'));

        if (!$mail_library->sendMail($mail)) {
            $output['alert'] = ['alert' => 'error', 'message' => str_replace('{{email}}', $email, Language::get('account/recovery_not_sent'))];
            exit(Output::json($output));
        }

        $output = ['alert' => 'success', 'message' => str_replace('{{email}}', $email, Language::get('account/recovery_sent'))];
        Output::json($output);
    }

    /**
     * Display the password reset page
     * 
     * @param string $token - A token generated by a method in this controller class @see $this->sendResetRequest()
     * @return void
     */
    public function reset($token = null)
    {
        $session = Session::get('reset');
        $invalid = false; 

        if (is_null($session)) $invalid = true;
        if (is_null($token)) $invalid = true;
        if ($session['token'] != $token) $invalid = true;
        if (isset($session) && time() - strtotime($session['time']) > 5 * 60) {
            Session::delete('reset');
            $invalid = true;
        }

        $view['header'] = Load::controller('header')->index();
        $view['footer'] = Load::controller('footer')->index();
        $view['invalid_token'] = Language::get('account/invalid_token_text');
        $view['invalid'] = $invalid;

        Output::html('account/reset', $view);
    }

    /**
     * Save a new password
     *
     * @return void
     */
    public function saveNewPassword()
    {
        if (!empty($_POST['password']) && !empty($_POST['confirm'])) {
            $password = $_POST['password'];
            $confirm = $_POST['confirm'];
            $session = Session::get('reset');

            if (Auth::isLogged()) {
                $email = Auth::email();
            }

            if (!Auth::isLogged() && isset($session)) {
                $email = $session['email'];
            }

            if (Load::model('settings')->getSetting('strong_pw')) {
                $pw_strong = isPasswordStrong($password);
                if (!$pw_strong) {
                    $output = ['alert' => 'error', 'message' => Language::get('account/password_weak')];
                    exit(Output::json($output));
                }
            }

            if ($password != $confirm) {
                $output = ['alert' => 'error', 'message' => Language::get('account/password_match')];
                exit(Output::json($output));
            }

            $password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
            $model = Load::model('user');
            $user = $model->getUser('email', $email);

            $data['password'] = $password;
            $data['email'] = $email;

            if ($model->updateUser($data, 'email')) {
                Log::event(Language::get('account/log_password_reset', ['name' => $user['firstname'] . ' ' . $user['lastname']]));
                $output = ['alert' => 'success', 'message' => Language::get('account/password_changed')];
            }
        }

        if (empty($_POST['password']) || empty($_POST['confirm'])) {
            $output = ['alert' => 'error', 'message' => Language::get('account/password_match')];
        }

        Output::json($output);
    }

    /**
     * Live validation
     *
     * Validate user input as they type. This method should be called via ajax.
     * 
     * @see root/public/javascript/validate.js
     * @see roo/public/html/account/login.htm
     * @return void
     */
    public function validateLive()
    {
        if (!empty($_POST['firstname']) && !Validate::alpha($_POST['firstname'])) {
            $this->error['firstname'] = Language::get('account/firstname_invalid');
        }
        if (!empty($_POST['lastname']) && !Validate::alpha($_POST['lastname'])) {
            $this->error['lastname'] = Language::get('account/lastname_invalid');
        }
        if (!empty($_POST['phone']) && !Validate::tel($_POST['phone'])) {
            $this->error['phone'] = Language::get('account/phone_invalid');
        }
        if (!empty($_POST['email']) && !Validate::email($_POST['email'])) {
            $this->error['email'] = Language::get('account/email_invalid');
        }
        if (!empty($_POST['password']) && !Validate::password($_POST['password'])) {
            if (Load::model('settings')->getSetting('strong_pw')) {
                $this->error['password'] = Language::get('account/password_weak');
            }
        }
        if (!empty($_POST['password']) && !empty($_POST['confirm'])) {
            if ($_POST['password'] !== $_POST['confirm']) {
                $this->error['confirm'] = Language::get('account/password_match');
            }
        } 

        if (isset($this->error)) {
            Output::json(['errors' => $this->error]);
        }
    }

    /**
     * Change users theme setting
     * 
     * Gusto users all get a light and dark theme that they may change
     *
     * @return void
     */
    public function changeThemeSetting() 
    {      
        $data['user_id'] = Auth::id();
        $data['theme'] = $_POST['theme'];

        Load::model('user')->updateUser($data, 'user_id');

        Output::json($data['theme']);
    }
}
