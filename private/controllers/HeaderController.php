<?php 

/**
 * Header Controller Class
 *
 * The HeaderController handles logic specific to the header and displays the 
 * header view. The HeaderController should be loaded in each controller class 
 * where a header is desired.
 */
class HeaderController extends Controller
{
    /**
     * Index method
     *
     * The index methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Assuming that the HeaderController should be loaded in every controller 
     * this index method should run in every controller too.
     */
    public function index($title = null)
    {    
        $url = isset($_GET['url']) ? splitUrl($_GET['url']) : [];

        $view['title'] = isset($title) ? $title : Language::get($url[0] . '/title');
        $view['logged'] = Auth::isLogged();
        $view['view_text'] = Language::get('header/view_text');
        $view['account_text'] = Language::get('header/account_text');
        $view['search'] = Load::view('common/search');
        $view['theme'] = '';

        if (Auth::isLogged()) {
            $view['theme'] = Auth::user()->theme == 1 ? 'dark-theme' : 'light-theme';
            $view['name'] = Auth::firstname() . ' ' . Auth::lastname();
            $view['avatar'] = '/uploads/account/' . Auth::user()->avatar;
            Load::model('user')->updateUser(['last_active' => date('c'), 'user_id' => Auth::id()], 'user_id');
        }

        return Load::view('common/header', $view);
    }
}