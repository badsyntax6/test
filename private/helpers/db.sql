-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 04, 2020 at 08:29 AM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `techutks_gusto`
--

-- --------------------------------------------------------

--
-- Table structure for table `calendar`
--

DROP TABLE IF EXISTS `calendar`;
CREATE TABLE IF NOT EXISTS `calendar` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `background` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `calendar`
--

INSERT INTO `calendar` (`event_id`, `title`, `description`, `start_time`, `end_time`, `color`, `background`) VALUES
(1, 'Hike', 'Going hiking with people', '2020-02-26T06:00:00-10:00', '2020-02-26T17:00:00-10:00', '#142f36', '#b9dae2'),
(2, 'Dentist', 'Getting cavities filled', '2020-02-15T11:30:00-10:00', '2020-02-15T13:30:00-10:00', '#383715', '#fbf779'),
(3, 'Star Wars', 'All day star wars marathon on TV', '2020-02-08T00:00:00-10:00', '2020-02-13T23:00:00-10:00', '#000000', '#ec4646'),
(4, 'Beach Weekend', 'Waimanalo with the homies', '2020-02-18T06:00:00-10:00', '2020-02-19T22:00:00-10:00', '#2a1538', '#a978f2'),
(29, 'Foomanchoo', 'Strippers at the foomanchoo club', '2020-02-01T01:00:00-10:00', '2020-02-01T03:00:00-10:00', '#2d1538', '#c679e7'),
(6, 'Orgy', 'End of month orgy', '2020-01-30T20:00:00-10:00', '2020-01-30T22:00:00-10:00', '#000000', '#e892c8'),
(7, 'STD Test', 'Getting tested for the HIV', '2020-01-30T14:00:00-10:00', '2020-01-30T15:00:00-10:00', NULL, NULL),
(8, 'Divorce', 'Tell the wife Im divorcing her', '2020-01-30T10:00:00-10:00', '2020-01-30T10:30:00-10:00', NULL, NULL),
(9, 'Circumcision', 'Snip snip', '2020-01-30T08:00:00-10:00', '2020-01-30T10:00:00-10:00', NULL, NULL),
(15, 'Foo', 'Foo', '2020-01-31T08:00:00-10:00', '2020-02-04T17:00:00-10:00', '#142f36', '#b9dae2'),
(13, 'Quit Job', 'Maybe just stop showing up?', '2020-01-30T00:00:00-10:00', '2020-01-30T23:00:00-10:00', '#153817', '#86df8c'),
(14, 'Suicide', 'End it all for the luls', '2020-01-30T23:00:00-10:00', '2020-01-30T23:30:00-10:00', NULL, NULL),
(12, 'Buy Condoms', 'Stop buy the needle and knife store and get some condoms for orgy tonight', '2020-01-30T12:00:00-10:00', '2020-01-30T13:00:00-10:00', NULL, NULL),
(16, 'Bar', 'Bar', '2020-01-31T08:00:00-10:00', '2020-01-31T17:00:00-10:00', NULL, NULL),
(17, 'Baz', 'Baz', '2020-01-31T08:00:00-10:00', '2020-01-31T17:00:00-10:00', NULL, NULL),
(18, 'Fin', 'Fin', '2020-01-31T08:00:00-10:00', '2020-01-31T17:00:00-10:00', NULL, NULL),
(19, 'Fan', 'Fan', '2020-01-31T08:00:00-10:00', '2020-01-31T17:00:00-10:00', NULL, NULL),
(20, 'Foom', 'Foom', '2020-01-31T08:00:00-10:00', '2020-01-31T17:00:00-10:00', NULL, NULL),
(21, 'Lorem', 'Lorem', '2020-01-31T08:00:00-10:00', '2020-01-31T17:00:00-10:00', NULL, NULL),
(22, 'Ipsum', 'Ipsum', '2020-01-31T08:00:00-10:00', '2020-01-31T17:00:00-10:00', NULL, NULL),
(23, 'Dolar', 'Dolar', '2020-01-31T08:00:00-10:00', '2020-01-31T17:00:00-10:00', NULL, NULL),
(24, 'Sit', 'Sit', '2020-01-31T08:00:00-10:00', '2020-01-31T17:00:00-10:00', NULL, NULL),
(25, 'Amet', 'Amet', '2020-01-31T08:00:00-10:00', '2020-01-31T17:00:00-10:00', NULL, NULL),
(26, 'Peepee', 'Peepee', '2020-01-31T08:00:00-10:00', '2020-01-31T17:00:00-10:00', NULL, NULL),
(27, 'Ping pong', 'Pingpong', '2020-01-31T08:00:00-10:00', '2020-01-31T17:00:00-10:00', NULL, NULL),
(28, 'Tab Meetings', 'All day tab meetings at hotel', '2020-02-15T00:00:00-10:00', '2020-02-15T23:00:00-10:00', '#142f36', '#b9dae2'),
(30, 'Christmas Break', 'Christmas break going on vacation.', '2020-02-23T00:00:00-10:00', '2020-02-29T23:00:00-10:00', '#ffffff', '#057632'),
(31, 'Penis Juice', 'Jizzin all over de plaze', '2020-02-06T14:00:00-10:00', '2020-02-06T15:00:00-10:00', '#142f36', '#a3c7f5');

-- --------------------------------------------------------

--
-- Table structure for table `errors`
--

DROP TABLE IF EXISTS `errors`;
CREATE TABLE IF NOT EXISTS `errors` (
  `error_id` int(11) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `event` text,
  PRIMARY KEY (`error_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
CREATE TABLE IF NOT EXISTS `language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`language_id`, `code`, `language`) VALUES
(1, 'en', 'English'),
(2, 'ja', 'Japanese');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `time` varchar(255) DEFAULT NULL,
  `event` text,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`log_id`, `time`, `event`) VALUES
(1, '2020-02-03T22:28:10-10:00', 'User (chase@techsourcehawaii.com) logged in.');

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

DROP TABLE IF EXISTS `logins`;
CREATE TABLE IF NOT EXISTS `logins` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `lock_time` varchar(255) NOT NULL,
  `attempts` int(2) NOT NULL,
  PRIMARY KEY (`login_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mail`
--

DROP TABLE IF EXISTS `mail`;
CREATE TABLE IF NOT EXISTS `mail` (
  `mail_id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) DEFAULT NULL,
  `port` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`mail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mail`
--

INSERT INTO `mail` (`mail_id`, `host`, `port`, `username`, `password`) VALUES
(1, 'smtp.office365.com', '587', 'chase@yourtechconnection.com', 'p@ssw0rd');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_anchor` int(11) NOT NULL,
  `main_menu` tinyint(4) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`menu_id`, `menu_anchor`, `main_menu`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `resets`
--

DROP TABLE IF EXISTS `resets`;
CREATE TABLE IF NOT EXISTS `resets` (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `creation_date` varchar(255) NOT NULL,
  PRIMARY KEY (`link_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_id`, `name`, `value`) VALUES
(1, 'owners_email', 'badsyntaxx@gmail.com'),
(2, 'strong_pw', NULL),
(3, 'inactivity_limit', '5'),
(4, 'language', 'en'),
(5, 'maintenance_mode', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `group` tinyint(1) NOT NULL DEFAULT '0',
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  `birthday` varchar(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `privacy` tinyint(1) NOT NULL DEFAULT '0',
  `avatar` varchar(255) NOT NULL DEFAULT 'default_avatar.png',
  `bio` text,
  `signup_date` varchar(255) NOT NULL,
  `last_active` varchar(255) DEFAULT NULL,
  `ip` varchar(255) NOT NULL,
  `verify_logout` tinyint(1) NOT NULL DEFAULT '1',
  `theme` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `key`, `group`, `firstname`, `lastname`, `username`, `email`, `password`, `website`, `birthday`, `country`, `gender`, `privacy`, `avatar`, `bio`, `signup_date`, `last_active`, `ip`, `verify_logout`, `theme`) VALUES
(1, 'dff976d884e5f65934f7392c88e6c673', 5, 'Chase', 'Asahina', '', 'chase@techsourcehawaii.com', '$2y$12$86a06NLrpim6phJf0gtpauyQKQJVSYPIPhXcDPLwKRfT9tpChjt2i', NULL, NULL, NULL, NULL, 0, 'default_avatar.png', NULL, '2020-02-03T22:27:51-10:00', '2020-02-03T22:28:14-10:00', '::1', 1, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
