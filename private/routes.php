<?php 

$routes = [
    // CALENDAR
    ["route" => "calendar",                         "controller" => "CalendarController",         "action" => "index",                  "level" => 2],
    ["route" => "calendar/new",                     "controller" => "CalendarController",         "action" => "add",                    "level" => 2],
    ["route" => "calendar/get-events",              "controller" => "CalendarController",         "action" => "getEvents",              "level" => 2],
    ["route" => "calendar/getEvent",                "controller" => "CalendarController",         "action" => "getEvent",               "level" => 2],
    ["route" => "calendar/getEventJson/{\d+}",      "controller" => "CalendarController",         "action" => "getEventJson",           "level" => 2],
    ["route" => "calendar/getCalendar",             "controller" => "CalendarController",         "action" => "getCalendar",            "level" => 2],
    ["route" => "calendar/edit/{\d+}",              "controller" => "CalendarController",         "action" => "edit",                   "level" => 2],
    ["route" => "calendar/update",                  "controller" => "CalendarController",         "action" => "update",                 "level" => 2],
    ["route" => "calendar/save",                    "controller" => "CalendarController",         "action" => "save",                   "level" => 2],
    ["route" => "calendar/delete",                  "controller" => "CalendarController",         "action" => "deleteEvent",            "level" => 2],
    // FILEBROWSER
    ["route" => "filebrowser",                      "controller" => "FilebrowserController",      "action" => "index",                  "level" => 2],
    ["route" => "filebrowser/browse",               "controller" => "FilebrowserController",      "action" => "browse",                 "level" => 2],
    ["route" => "filebrowser/makeFolder",           "controller" => "FilebrowserController",      "action" => "makeFolder",             "level" => 2],
    ["route" => "filebrowser/delete",               "controller" => "FilebrowserController",      "action" => "delete",                 "level" => 2],
    ["route" => "filebrowser/upload",               "controller" => "FilebrowserController",      "action" => "upload",                 "level" => 2],
    // TEST
    ["route" => "test",                             "controller" => "TestController",             "action" => "index",                  "level" => 0],
    ["route" => "test/validate",                    "controller" => "TestController",             "action" => "validate",               "level" => 0],
    ["route" => "test/getDate",                     "controller" => "TestController",             "action" => "getDate",                "level" => 0],
    ["route" => "test/json",                        "controller" => "TestController",             "action" => "json",                   "level" => 0],
    ["route" => "test/test",                        "controller" => "TestController",             "action" => "test",                   "level" => 0],
    ["route" => "test/test-live",                   "controller" => "TestController",             "action" => "testLive",               "level" => 0],
];

return $routes;